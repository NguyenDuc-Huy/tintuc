<footer id="footer">
    <div class="footer_top">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_widget wow fadeInLeftBig">

                    <a href="index.php" class="logo">
                        <img class="rounded-3" style="margin: 15px 0;" src="./public/images/logo7.png" alt="" height="80">
                    </a>
                    <p><b>Báo tiếng Việt nhiều người xem nhất</b></p>
                    <p>Thuộc Bộ Khoa học Công nghệ </p>
                    <p>Số giấy phép: 06/GP-BTTTT ngày 03/01/2014</p>
                    <ul class="media_footer">
                        <li><a class="fb" href=""><i class="fa fa-facebook-square"></i></a> </li>
                        <li> <a class="tw" href=""> <i class="fa fa-twitter-square"></i></a></li>
                        <li> <a class="pi" href=""> <i class="fa fa-pinterest-square"></i></a></li>
                        <li> <a class="gg" href=""> <i class="fa fa-google-plus-square"></i></a></li>
                    </ul>

                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_widget wow fadeInDown">
                    <h2>Đơn vị liên kết</h2>
                    <ul class="tag_nav">
                        <li><a href="#">NDH Company, USA</a></li>
                        <li><a href="#">IT facalty, Nha Trang University</a></li>
                        <li><a href="#">Mr. Hai Trieu Nguyen</a></li>
                        <li><a href="#">VnExpress.com</a></li>
                        <li><a href="#">Youtube.com</a></li>

                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_widget wow fadeInRightBig">
                    <h2>Liên hệ</h2>
                    <p>Tổng biên tập: Nguyễn Đức Huy </p>
                    <p>Điện thoại: 024 7300 8899</p>
                    <p>Email: <a style="color: white;" href="mailto:duchuyit2@gmail.com">duchuyit2@gmail.com</a></p>
                    <address>
                        Địa chỉ: Ninh Hưng, Tx. Ninh Hòa, Khánh Hòa
                    </address>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <p class="copyright">All copyright &copy; <?php $date = getdate();
                                                    echo $date['year'] ?> <a href="index.html">Tin tức hằng ngày 24h</a></p>
        <p class="developer">Developed By Nguyen Duc Huy</p>
    </div>
</footer>
</div>
<script src="public/assets/js/jquery.min.js"></script>
<script src="public/assets/js/wow.min.js"></script>
<script src="public/assets/js/bootstrap.min.js"></script>
<script src="public/assets/js/slick.min.js"></script>
<script src="public/assets/js/jquery.li-scroller.1.0.js"></script>
<script src="public/assets/js/jquery.newsTicker.min.js"></script>
<script src="public/assets/js/jquery.fancybox.pack.js"></script>
<script src="public/assets/js/custom.js"></script>


</body>

</html>