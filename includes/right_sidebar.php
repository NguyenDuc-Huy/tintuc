 <div class="col-lg-4 col-md-4 col-sm-4">
     <aside class="right_content">
         <div class="single_sidebar">
             <h2><span>Tin xem nhiều</span></h2>
             <ul class="spost_nav">
                 <?php
                    $tinxn = $tin->showTinPop();
                    if ($tinxn) {
                        while ($tinxn_e = $tinxn->fetch_assoc()) {
                    ?>
                         <li>
                             <div class="media wow fadeInDown"> <a href="tin.php?tinId=<?= $tinxn_e['tinId'] ?>" class="media-left"> <img alt="" src="admin/upload/images/<?php echo $tinxn_e['hinh']; ?>"> </a>
                                 <div class="media-body"> <a href="tin.php?tinId=<?= $tinxn_e['tinId'] ?>" class="catg_title"> <?php echo $tinxn_e['tieude']; ?>"</a> </div>
                             </div>
                         </li>
                 <?php }
                    } ?>

             </ul>
         </div>
         <div class="single_sidebar">
             <ul class="nav nav-tabs" role="tablist">
                 <li role="presentation" class="active"><a href="#video" aria-controls="profile" role="tab" data-toggle="tab">Video</a></li>
                 <li role="presentation"><a href="#category" aria-controls="home" role="tab" data-toggle="tab">Loại tin</a></li>

                 <li role="presentation"><a href="#comments" aria-controls="messages" role="tab" data-toggle="tab">Bình luận</a></li>
             </ul>
             <div class="tab-content">
                 <div role="tabpanel" class="tab-pane active" id="video">
                     <div class="vide_area">
                         <iframe width="100%" height="250" src="https://www.youtube.com/embed/FX0u2m0YcGo" frameborder="0" allowfullscreen></iframe>
                     </div>
                 </div>
                 <div role="tabpanel" class="tab-pane" id="category">
                     <ul>
                         <?php
                            $lt = $loaitin->showLT();
                            if ($lt) {
                                while ($row = $lt->fetch_assoc()) {
                            ?>
                                 <li class="cat-item"><a href="loaitinpage.php?loaitinId=<?= $row['loaitinId'] ?>"><?php echo $row['tenloaitin'] ?></a></li>
                         <?php }
                            } ?>
                     </ul>
                 </div>

                 <div role="tabpanel" class="tab-pane" id="comments">
                     <ul class="spost_nav">
                         <li>
                             <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>
                                 <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 1</a> </div>
                             </div>
                         </li>


                     </ul>
                 </div>
             </div>
         </div>
         <div class="single_sidebar wow fadeInDown">
             <h2><span>Nhà tài trợ</span></h2>
             <a class="sideAdd" href="#"><img src="./public/images/sponsor.jpg" alt=""></a>
         </div>
         <div class="single_sidebar wow fadeInDown">
             <h2><span>Thể loại nổi bật</span></h2>
             <select class="catgArchive">
                 <option>--Chọn thể loại--</option>
                 <?php
                    $result = $theloai->showTL();
                    if ($result) {
                        while ($row = $result->fetch_assoc()) {
                            echo '<option><a href="theloaipage.php?theloaiId=' . $row['theloaiId'] . '">' . $row['tentheloai'] . '</a></option>';
                        }
                    }
                    ?>


             </select>
         </div>

         <div class="single_sidebar wow fadeInDown">

             <a class="sideAdd" href="#"><img src="public/images/trex.gif" alt=""></a>
             <a class="sideAdd" href="#"><img src="public/images/tanminhchi.gif" alt=""></a>
             <a class="sideAdd" href="#"><img src="public/images/tiger.gif" alt=""></a>
         </div>
     </aside>
 </div>