<?php
session_start();
function get_current_weekday()
{
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    $weekday = date("l");
    $weekday = strtolower($weekday);
    switch ($weekday) {
        case 'monday':
            $weekday = 'Thứ hai';
            break;
        case 'tuesday':
            $weekday = 'Thứ ba';
            break;
        case 'wednesday':
            $weekday = 'Thứ tư';
            break;
        case 'thursday':
            $weekday = 'Thứ năm';
            break;
        case 'friday':
            $weekday = 'Thứ sáu';
            break;
        case 'saturday':
            $weekday = 'Thứ bảy';
            break;
        default:
            $weekday = 'Chủ nhật';
            break;
    }
    return $weekday . ', ' . date('d/m/Y');
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>Tin tức hằng ngày 24h</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="public/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="public/assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="public/assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="public/assets/css/font.css">
    <link rel="stylesheet" type="text/css" href="public/assets/css/li-scroller.css">
    <link rel="stylesheet" type="text/css" href="public/assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="public/assets/css/jquery.fancybox.css">
    <link rel="stylesheet" type="text/css" href="public/assets/css/color.css">
    <link rel="stylesheet" type="text/css" href="public/assets/css/my_style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>
    <!-- <div id="preloader">
        <div id="status">&nbsp;</div>
    </div> -->
    <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
    <div class="container">
        <header id="header">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="header_top">
                        <div class="header_top_left">
                            <ul class="top_nav">
                                <li><a href="index.php">Trang chủ</a></li>
                                <li><a href="myInfo.php">Thông tin sinh viên</a></li>
                                <li><a href="baitap.php">Bài tập</a></li>
                            </ul>
                        </div>
                        <div class="header_top_right">
                            <?php if (isset($_SESSION['username'])) {
                            ?>
                                <p class="top_nav"><i style="color: red;font-size:15px"><?php echo $_SESSION['username']; ?></i>&nbsp;&nbsp;<a href="admin/logout.php" style="color: white;">Đăng xuất</a>
                                </p>
                            <?php } else echo '<p class="top_nav"><a href="login.php" style="color: white;">Đăng nhập</a>
                                </p>'; ?>
                        </div>

                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="header_bottom">
                        <div class="logo_area">
                            <a href="index.php" class="logo">
                                <img src="public/images/logo7.png" alt="" height="100">
                            </a>
                            <span class="ngaythang"><?php echo get_current_weekday(); ?></span>
                        </div>
                        <div class="add_banner"><a href="#"><img src="public/images/quangcao.gif" alt=""></a></div>
                    </div>

                </div>
            </div>
        </header>
        <section id="navArea">
            <nav class="navbar navbar-inverse" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav main_nav">
                        <li class="active"><a href="index.php"><span style="font-size: 30px;" class="fa fa-home desktop-home"></span><span class="mobile-show">Home</span></a></li>
                        <?php

                        include 'classes/theloai_class.php';
                        include './classes/loaitin_class.php';
                        include './classes/bantin_class.php';
                        $theloai =  new theloai();
                        $loaitin = new loaitin();
                        $result = $theloai->showTL();
                        if ($result) {
                            while ($row = $result->fetch_assoc()) {

                        ?>
                                <li class="dropdown"><a href="theloaipage.php?theloaiId=<?= $row['theloaiId'] ?>" class="dropdown-toggle" role="button" aria-expanded="false"><?php echo $row['tentheloai'] ?></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <?php $lt = $loaitin->getLTTL($row['theloaiId']);
                                        if ($lt) {
                                            while ($res = $lt->fetch_assoc()) {
                                                echo '<li><a href="loaitinpage.php?loaitinId=' . $res['loaitinId'] . '">' . $res['tenloaitin'] . '</a></li>';
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>

                        <?php
                            }
                        }
                        ?>
                    </ul>
                    <!--  data-toggle="dropdown"-->
                </div>
            </nav>
        </section>
        <section id="newsSection">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="latest_newsarea"> <span>Tin mới nhất</span>
                        <ul id="ticker01" class="news_sticker">
                            <?php
                            $bantin  = new bantin();
                            $result3 = $bantin->showLastedNew();
                            if ($result3) {
                                while ($res2 = $result3->fetch_assoc()) {
                                    echo ' <li><a href="tin.php?tinId=' . $res2['tinId'] . '"><img width="25" height="20" src="admin/upload/images/' . $res2['hinh'] . '" alt="">' . $res2['tieude'] . '</a></li>';
                                }
                            }
                            ?>
                        </ul>
                        <div class="social_area">
                            <ul class="social_nav">
                                <li class="facebook"><a href="https://www.facebook.com/Huy.Amory.1238/"></a></li>
                                <li class="twitter"><a href="https://twitter.com/?lang=vi"></a></li>
                                <li class="flickr"><a href="#"></a></li>
                                <li class="pinterest"><a href="#"></a></li>
                                <li class="googleplus"><a href="#"></a></li>
                                <li class="vimeo"><a href="#"></a></li>
                                <li class="youtube"><a href="#"></a></li>
                                <li class="mail"><a href="#"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>