<!doctype html>
<html lang="en">

<head>
    <title>Quên mật khẩu?</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="./public/assets/css/login_style.css">
</head>
<?php
include './classes/check_login.php';
$check = new check_login();
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) {
    $login = $check->forgetPws($_POST);
}
?>

<body>
    <div class="container mt-3">
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4">
                <div class="login-wrap py-5">
                    <div class="img d-flex align-items-center justify-content-center" style="background-image: url(./public/images/bg.png);"></div>
                    <h3 class="text-center mb-0">Welcome</h3>
                    <p class="text-center">Nhập thông tin để cấp lại mật khẩu</p>
                    <p class="text-center" style="color: green">Mật khẩu mới sẽ được gửi vào email của bạn. Vui lòng kiểm tra email để tiếp tục đăng nhập!</p>
                    <form action="" method="POST" class="login-form">
                        <div class="form-group">
                            <div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-user"></span></div>
                            <input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo isset($_POST['username']) ? $_POST['username'] : "" ?>" required>
                        </div>
                        <div class="form-group">
                            <div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-envelope"></span></div>
                            <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : "" ?>" required>
                        </div>

                        <div class="form-group d-md-flex">
                            <div class="w-100 text-md-center <?php echo isset($login) ? "" : "d-none" ?>">
                                <?php if (isset($login)) echo '<i style="color:red">' . $login  . '</i>' ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn form-control btn-primary rounded submit px-3">Xác nhận</button>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</body>

</html>