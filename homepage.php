<section id="sliderSection">
    <div class="row">

        <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="slick_slider">
                <?php
                $tin = new bantin();
                $tinslide = $tin->showTintxt();
                if ($tinslide) {
                    while ($rowSl = $tinslide->fetch_assoc()) {
                ?>
                        <div class="single_iteam"> <a href="tin.php?tinId=<?= $rowSl['tinId'] ?>"> <img src="admin/upload/images/<?php echo $rowSl['hinh']; ?>" alt=""></a>
                            <div class="slider_article">
                                <h2><a class="slider_tittle" href="tin.php?tinId=<?= $rowSl['tinId'] ?>"><?php echo $rowSl['tieude']; ?></a></h2>
                                <?php echo $rowSl['tomtat']; ?>
                            </div>
                        </div>

                <?php
                    }
                }
                ?>

            </div>
        </div>
        <div class="col-sm-4 text-right" style="margin-bottom:10px">
            <form class="form-inline" method="GET" action="timkiem.php">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" size="30" name="tukhoa" value="<?php if (isset($_POST['tukhoa'])) echo $_POST['tukhoa']; ?>" placeholder="Nhập từ khóa tìm kiếm">
                        <div class="input-group-addon"><button type="submit" name="submit" style="border: none; "><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="latest_post">
                <h2><span>Bài viết nổi bật</span></h2>
                <div class="latest_post_container">
                    <div id="prev-button"><i class="fa fa-chevron-up"></i></div>
                    <ul class="latest_postnav">
                        <?php
                        $tinnb = $tin->showFeaturePost();
                        if ($tinnb) {
                            while ($tinnb_e = $tinnb->fetch_assoc()) {
                        ?>
                                <li>
                                    <div class="media"> <a href="tin.php?tinId=<?= $tinnb_e['tinId'] ?>" class="media-left"> <img alt="" src="admin/upload/images/<?php echo $tinnb_e['hinh']; ?>"> </a>
                                        <div class="media-body"> <a href="tin.php?tinId=<?= $tinnb_e['tinId'] ?>" class="catg_title"><?php echo $tinnb_e['tieude']; ?></a> </div>
                                    </div>
                                </li>
                        <?php
                            }
                        }
                        ?>

                    </ul>
                    <div id="next-button"><i class="fa  fa-chevron-down"></i></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="contentSection">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="left_content">
                <div class="single_post_content">
                    <h2><span>Giáo dục</span></h2>
                    <div class="single_post_content_left">
                        <ul class="business_catgnav  wow fadeInDown">
                            <?php
                            $tingd = $tin->showTinXX('Giáo dục', 1, 0);
                            if ($tingd) {
                                while ($r = $tingd->fetch_assoc()) {
                            ?>
                                    <li>
                                        <figure class="bsbig_fig text-justify"> <a href="tin.php?tinId=<?= $r['tinId'] ?>" class="featured_img"> <img alt="" src="admin/upload/images/<?php echo $r['hinh']; ?>"> <span class="overlay"></span> </a>
                                            <figcaption> <a href="tin.php?tinId=<?= $r['tinId'] ?>"><?php echo $r['tieude'] ?></a> </figcaption>
                                            <?php echo $r['tomtat']; ?>
                                        </figure>
                                    </li>
                            <?php }
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="single_post_content_right">
                        <ul class="spost_nav">
                            <?php
                            $list_tingd = $tin->showTinXX('Giáo dục', 4, 2);
                            if ($list_tingd) {
                                while ($r = $list_tingd->fetch_assoc()) {
                            ?>
                                    <li>
                                        <div class="media wow fadeInDown"> <a href="tin.php?tinId=<?= $r['tinId'] ?>" class="media-left"> <img alt="" src="admin/upload/images/<?php echo $r['hinh']; ?>"> </a>
                                            <div class="media-body"> <a href="tin.php?tinId=<?= $r['tinId'] ?>" class="catg_title"> <?php echo $r['tieude'] ?></a> </div>
                                        </div>
                                    </li>
                            <?php
                                }
                            } ?>
                        </ul>
                    </div>
                </div>
                <div class="fashion_technology_area">
                    <div class="fashion">
                        <div class="single_post_content">
                            <h2><span>Kinh doanh</span></h2>
                            <?php
                            $tinkd = $tin->showTinXX('Kinh doanh', 1, 0);
                            if ($tinkd) {
                                $tinkd_e = $tinkd->fetch_assoc()
                            ?>
                                <ul class="business_catgnav wow fadeInDown">
                                    <li>
                                        <figure class="bsbig_fig"> <a href="tin.php?tinId=<?= $tinkd_e['tinId'] ?>" class="featured_img"> <img alt="" src="admin/upload/images/<?php echo $tinkd_e['hinh']; ?>"> <span class="overlay"></span> </a>
                                            <figcaption> <a href="tin.php?tinId=<?= $tinkd_e['tinId'] ?>"><?php echo $tinkd_e['tieude'] ?></a> </figcaption>
                                            <?php echo $tinkd_e['tomtat'] ?>
                                        </figure>
                                    </li>
                                </ul>
                            <?php
                            } ?>
                            <ul class="spost_nav">
                                <?php
                                $tinkdLlist = $tin->showTinXX('Kinh doanh', 3, 1);
                                if ($tinkdLlist) {
                                    while ($tinkd_e = $tinkdLlist->fetch_assoc()) {
                                ?>
                                        <li>
                                            <div class="media wow fadeInDown"> <a href="tin.php?tinId=<?= $tinkd_e['tinId'] ?>" class="media-left"> <img alt="" src="admin/upload/images/<?php echo $tinkd_e['hinh']; ?>"> </a>
                                                <div class="media-body"> <a href="tin.php?tinId=<?= $tinkd_e['tinId'] ?>" class="catg_title"> <?php echo $tinkd_e['tieude'] ?></a> </div>
                                            </div>
                                        </li>
                                <?php }
                                } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="technology">
                        <div class="single_post_content">
                            <h2><span>Thể thao</span></h2>
                            <?php
                            $tintt = $tin->showTinXX('Thể thao', 1, 0);
                            if ($tintt) {
                                $tintt_e = $tintt->fetch_assoc()
                            ?>
                                <ul class="business_catgnav wow fadeInDown">
                                    <li>
                                        <figure class="bsbig_fig"> <a href="tin.php?tinId=<?= $tintt_e['tinId'] ?>" class="featured_img"> <img alt="" src="admin/upload/images/<?php echo $tintt_e['hinh']; ?>"> <span class="overlay"></span> </a>
                                            <figcaption> <a href="tin.php?tinId=<?= $tintt_e['tinId'] ?>"><?php echo $tintt_e['tieude'] ?></a> </figcaption>
                                            <?php echo $tintt_e['tomtat'] ?>
                                        </figure>
                                    </li>
                                </ul>
                            <?php
                            } ?>
                            <ul class="spost_nav">
                                <?php
                                $tintt = $tin->showTinXX('Thể thao', 4, 1);
                                if ($tintt_e) {
                                    while ($tintt_e = $tintt->fetch_assoc()) {
                                ?>
                                        <li>
                                            <div class="media wow fadeInDown"> <a href="tin.php?tinId=<?= $tintt_e['tinId'] ?>" class="media-left"> <img alt="" src="admin/upload/images/<?php echo $tintt_e['hinh']; ?>"> </a>
                                                <div class="media-body"> <a href="tin.php?tinId=<?= $tintt_e['tinId'] ?>" class="catg_title"> <?php echo $tintt_e['tieude'] ?></a> </div>
                                            </div>
                                        </li>
                                <?php }
                                } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="single_post_content">
                    <h2><span>Sức khỏe - Ẩm thực</span></h2>
                    <ul class="photograph_nav  wow fadeInDown">
                        <?php
                        $tinvi = $tin->showTinVideo();
                        if ($tinvi) {
                            while ($tinvi_e = $tinvi->fetch_assoc()) {

                        ?>
                                <li>
                                    <div class="photo_grid">
                                        <figure class="effect-layla">
                                            <?php echo $tinvi_e['linkvideo'] ?>
                                        </figure>
                                    </div>
                                </li>
                        <?php
                            }
                        }
                        ?>

                    </ul>
                </div>
                <div class="single_post_content">
                    <h2><span>Giải trí</span></h2>
                    <div class="single_post_content_left">
                        <ul class="business_catgnav">
                            <?php
                            $tingt = $tin->showTinXX('Giải trí', 1, 0);
                            if ($tingt) {
                                while ($tingt_e = $tingt->fetch_assoc()) {

                            ?>
                                    <li>
                                        <figure class="bsbig_fig  wow fadeInDown"> <a class="featured_img" href="tin.php?tinId=<?= $tingt_e['tinId'] ?>"> <img src="admin/upload/images/<?php echo $tingt_e['hinh']; ?>" alt=""> <span class="overlay"></span> </a>
                                            <figcaption> <a href="tin.php?tinId=<?= $tingt_e['tinId'] ?>"> <?php echo $tingt_e['tieude'] ?></a> </figcaption>
                                            <?php echo $tingt_e['tomtat'] ?>
                                        </figure>
                                    </li>
                            <?php }
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="single_post_content_right">
                        <ul class="spost_nav">
                            <?php
                            $tingt = $tin->showTinXX('Giải trí', 4, 1);
                            if ($tingt) {
                                while ($tingt_e = $tingt->fetch_assoc()) {

                            ?>
                                    <li>
                                        <div class="media wow fadeInDown"> <a href="tin.php?tinId=<?= $tingt_e['tinId'] ?>" class="media-left"> <img alt="" src="admin/upload/images/<?php echo $tingt_e['hinh']; ?>"> </a>
                                            <div class="media-body"> <a href="tin.php?tinId=<?= $tingt_e['tinId'] ?>" class="catg_title"><?php echo $tingt_e['tieude'] ?></a> </div>
                                        </div>
                                    </li>
                            <?php }
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php include './includes/right_sidebar.php'; ?>
    </div>
</section>