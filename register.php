<!doctype html>
<html lang="en">

<head>
    <title>Register</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="./public/assets/css/login_style.css">
</head>
<?php
include './classes/docgia_class.php';
$dg = new docgia();
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) {
    $check = $dg->insertDG($_POST);
}
?>

<body>
    <div class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="login-wrap py-3 row">
                    <div class="img d-flex align-items-center justify-content-center" style="background-image: url(./public/images/bg.png);"></div>
                    <h3 class="text-center mb-0 col-sm-12">Đăng ký</h3>
                    <p class="text-center col-sm-12">Nhập đầy đủ các thông tin</p>
                    <form action="" method="POST" class="login-form col-sm-12">
                        <div class="form-group ">
                            <div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-user"></span></div>
                            <input type="text" name="name" class="form-control" placeholder="Username" value="<?php echo isset($_POST['name']) ? $_POST['name'] : "" ?>" required>
                        </div>
                        <div class="form-group">
                            <div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-envelope"></span></div>
                            <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : "" ?>" required>
                        </div>
                        <div class="form-group">
                            <div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-lock"></span></div>
                            <input type="password" name="password" class="form-control" placeholder="Mật khẩu" value="<?php echo isset($_POST['password']) ? $_POST['password'] : "" ?>" required>
                        </div>
                        <div class="form-group">
                            <div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-lock"></span></div>
                            <input type="password" name="re_password" class="form-control" placeholder="Nhập lại mật khẩu" value="<?php echo isset($_POST['re_password']) ? $_POST['re_password'] : "" ?>" required>
                        </div>

                        <div class="form-group d-md-flex">
                            <div class="w-100 text-md-center <?php echo isset($check) ? "" : "d-none" ?>">
                                <?php if (isset($check)) echo '<i style="color:red">' . $check  . '</i>' ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn form-control btn-primary rounded submit px-3">Đăng ký</button>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</body>

</html>