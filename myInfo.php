<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Thông tin sinh viên</title>
    <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="./public/assets/css/Info_styles.css" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
        <a class="navbar-brand js-scroll-trigger">
            <span class="d-block d-lg-none">Thông tin sinh viên</span>
            <span class="d-none d-lg-block"><img class="img-fluid img-profile rounded-circle mx-auto mb-5" style="transform: scale(1.45);" src="./public/images/duchuy.jpg" alt="..." /></span>
        </a>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger">ID: 60135753</a>
                </li>

            </ul>
        </div>
    </nav>
    <!-- Page Content-->
    <div class="container-fluid p-0">
        <!-- About-->
        <section class="resume-section" id="about">
            <div class="resume-section-content">
                <h1 class="mb-0">
                    Nguyễn Đức
                    <span class="text-primary">Huy</span>
                </h1>
                <div class="subheading mb-5">
                    Thị xã Ninh Hòa · Khánh Hòa· (+84) 702 634 575 ·
                    <a href="mailto:duchuyit2@gmail.com">duchuyit2@gmail.com</a> · &copy 2021
                </div>
                <p class="lead">
                    Sinh viên năm 4, lớp 60.CNTT-2 Công nghệ phần mềm, khoa Công nghệ thông tin, trường đại học Nha Trang.
                </p>
                <p class="mb-5" style="text-align: right; font-size:14px; font-style:italic; color:#bd5d38"> "Looking up to see how short you are.
                    Looking down to know you are not high"
                </p>
                <div class="social-icons">
                    <a class="social-icon" href="#!"><i class="fab fa-linkedin-in"></i></a>
                    <a class="social-icon" href="#!"><i class="fab fa-github"></i></a>
                    <a class="social-icon" href="#!"><i class="fab fa-twitter"></i></a>
                    <a class="social-icon" href="https://www.facebook.com/Huy.Amory.1238/"><i class="fab fa-facebook-f"></i></a>
                </div>
            </div>
        </section>
    </div>

</body>

</html>