﻿<?php

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

$filepath = realpath(dirname(__FILE__));
include $filepath . '/../admin/source/PHPMailer/src/Exception.php';
require $filepath . '/../admin/source/PHPMailer/src/PHPMailer.php';
require $filepath . '/../admin/source/PHPMailer/src/SMTP.php';

// Instantiation and passing `true` enables exceptions
class Mailer
{

    public function sendMailCF($mTo, $nTo, $name, $pass)
    {
        $mail = new PHPMailer(true);
        $mail->isSMTP();
        $mail->CharSet = 'utf-8'; // gửi mail SMTP
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'duchuyit2@gmail.com';
        $mail->Password = '';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;

        //Recipients
        $mail->setFrom('duchuyit2@gmail.com', '[NO REPLY] Tin tức 24h');
        $mail->addAddress($mTo, $nTo);
        // $mail->addReplyTo('duchuyit2@gmail.com', 'No Reply');
        $mail->isHTML(true);   // Set email format to HTML
        $mail->Subject = 'TIN TỨC 24H - XÁC NHẬN THÔNG TIN TÀI KHOẢN';
        $mail->Body = '<h3>Chào mừng bạn đến với trang Tin tức 24h</h3>
    				<p>Tên đăng nhập: <b>' . $name . '</b></p>
       				<p>Mật khẩu: <b>' . $pass . '</b></p>';

        $mail->send();
    }
}


// $mail = sendMailCF('duchuy011220tdtce@gmail.com', 'Nhân viên', 'username', '123');
