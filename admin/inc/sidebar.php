  <aside class="left-sidebar" data-sidebarbg="skin5">
      <!-- Sidebar scroll-->
      <div class="scroll-sidebar">
          <!-- Sidebar navigation-->
          <nav class="sidebar-nav">
              <ul id="sidebarnav" class="pt-4">
                  <li class="sidebar-item">
                      <a class="sidebar-link waves-effect waves-dark sidebar-link active" href="index.php" aria-expanded="false"><i class="mdi mdi-home-variant"></i><span class="hide-menu">Trang chủ</span></a>
                  </li>
                  <li class="sidebar-item">
                      <a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php?module=bantin" aria-expanded="false"><i class="mdi mdi-new-box"></i><span class="hide-menu">Bản tin</span></a>
                  </li>
                  <!-- <li class="sidebar-item">
                      <a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php?module=loaitin" aria-expanded="false"><i class="mdi mdi-chart-bubble"></i><span class="hide-menu">Loại tin</span></a>
                  </li> -->
                  <li class="sidebar-item">
                      <a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php?module=theloai" aria-expanded="false"><i class="mdi mdi-blur-linear"></i><span class="hide-menu">Thể loại - Loại tin</span></a>
                  </li>

                  <li class="sidebar-item">

                      <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class=" mdi mdi-account-settings-variant"></i><span class="hide-menu">Người dùng</span></a>
                      <ul aria-expanded="false" class="collapse first-level">

                          <li class="sidebar-item">
                              <a href="index.php?module=nhanvien" class="sidebar-link"><i class="mdi mdi-account-edit"></i><span class="hide-menu"> Nhân viên</span></a>
                          </li>
                          <li class="sidebar-item">
                              <a href="index.php?module=khach" class="sidebar-link"><i class="mdi mdi-account-multiple"></i><span class="hide-menu"> Khách </span></a>
                          </li>
                      </ul>
                  </li>
                  <li class="sidebar-item">
                      <a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php?module=binhluan" aria-expanded="false"><i class="mdi mdi-comment"></i><span class="hide-menu">Bình luận</span></a>
                  </li>
                  <li class="sidebar-item">
                      <a class="sidebar-link waves-effect waves-dark sidebar-link" <?php if (isset($_SESSION['userId'])) echo 'href = "myAcc.php"';
                                                                                    else echo 'href = "404.php"';   ?> aria-expanded="false"><i class="mdi mdi-account-card-details"></i><span class="hide-menu">Tài khoản của tôi</span></a>
                  </li>
                  <li class="sidebar-item">
                      <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-chart-areaspline"></i><span class="hide-menu">Thống kê </span></a>
                      <ul aria-expanded="false" class="collapse first-level">
                          <li class="sidebar-item">
                              <a href="index.php?module=TK_sotin" class="sidebar-link"><i class="mdi mdi-newspaper"></i><span class="hide-menu"> Tin của thể loại</span></a>
                          </li>
                          <li class="sidebar-item">
                              <a href="index.php?module=TK_loaitinxemnhieu" class="sidebar-link"><i class="mdi mdi-trending-up"></i><span class="hide-menu"> Loại tin xem nhiều</span></a>
                          </li>

                          <li class="sidebar-item">
                              <a href="index.php?module=TK_docgiamoi" class="sidebar-link"><i class="mdi mdi-human-child"></i><span class="hide-menu"> Độc giả mới</span></a>
                          </li>
                      </ul>
                  </li>

                  <li class="sidebar-item">
                      <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-move-resize-variant"></i><span class="hide-menu">Cấu hình website</span></a>
                      <ul aria-expanded="false" class="collapse first-level">
                          <li class="sidebar-item">
                              <a href="index2.html" class="sidebar-link"><i class="mdi mdi-menu"></i><span class="hide-menu"> Thiết lập menu</span></a>
                          </li>
                          <li class="sidebar-item">
                              <a href="pages-gallery.html" class="sidebar-link"><i class="mdi mdi-projector-screen"></i><span class="hide-menu"> Thiết lập slides </span></a>
                          </li>
                          <li class="sidebar-item">
                              <a href="pages-calendar.html" class="sidebar-link"><i class="mdi mdi-view-agenda"></i><span class="hide-menu"> Thiết lập footer </span></a>
                          </li>

                      </ul>
                  </li>
                  <li class="sidebar-item">
                      <a class="sidebar-link waves-effect waves-dark sidebar-link" href="#" aria-expanded="false" data-toggle="modal" data-target="#logout"><i class="mdi mdi-exit-to-app"></i><span class="hide-menu">Thoát</span></a>
                  </li>



              </ul>
          </nav>
          <!-- End Sidebar navigation -->
      </div>
      <!-- End Sidebar scroll-->
  </aside>
  <div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">XÁC NHẬN</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  Bạn có muốn đăng xuất khỏi hệ thống?
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                  <a href="../logout.php" class="btn btn-primary">OK</a>
              </div>
          </div>
      </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>