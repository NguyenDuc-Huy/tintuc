<?php
include_once('../classes/bantin_class.php');
include_once('../classes/theloai_class.php');

$tl =  new theloai();
$tin = new bantin();
$tentl = $tl->showTL();
if ($tentl) {
    while ($res1 = $tentl->fetch_assoc()) {
        $ten[] = $res1['tentheloai'];
        $st =  $tin->SLtinTL($res1['theloaiId']);
        if ($st) {
            $res2 = $st->fetch_assoc();
            $index[] = (int) $res2['SL'];
        }
    }
}

$x = [$ten, $index];
echo json_encode($x);
