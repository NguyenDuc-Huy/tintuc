<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Thông tin nhân viên</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./bootstrap/assets/images/favicon.png" />
    <!-- Custom CSS -->

    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/quill/dist/quill.snow.css" />
    <link href="./bootstrap/dist/css/style.min.css" rel="stylesheet" />

    <link href="./bootstrap/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet" />
</head>
<?php
session_start();
include('../classes/nhanvien_class.php');
$nv = new nhanvien();

$id = $_SESSION['userId'];

?>

<body>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">

        <?php include "./inc/header.php"; ?>

        <?php include "./inc/sidebar.php"; ?>

        <div class="page-wrapper">
            <div class="card h-100">
                <div class="card-body">
                    <div class="row">
                        <?php
                        $list = $nv->getNV($id);
                        if ($list) {
                            $result = $list->fetch_assoc();

                        ?>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 font-16">
                                <div class="account-settings text-center">
                                    <div class="user-profile">
                                        <div class="user-avatar mt-5">
                                            <?php if ($result['anhdaidien'] == NULL)
                                                echo '<img class="rounded" src="./css/images/avatar.jpg" width="130px" alt="Avatar">';
                                            else echo '<img class="rounded" src="./upload/avatar/' . $result['anhdaidien'] . '" width="130px" alt="Avatar">';
                                            ?>

                                        </div>

                                    </div>
                                    <div>
                                        <p>
                                            Nghiệp vụ: <?php echo $result['chucvu'] == 1 ? "<b style='color:red'>Admin</b>" : "Nhân viên" ?>

                                        </p>

                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12 font-16">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <h2 class="mb-4 text-primary" style="margin-left: 200px;">THÔNG TIN CHI TIẾT</h2>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="fullName">Mã nhân viên: </label>
                                            <b><?php echo $result['Id']; ?></b>

                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="fullName">Họ tên nhân viên: </label>
                                            <b><?php echo $result['hotenNV']; ?></b>

                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="website">Giới tính: </label>
                                            <b>
                                                <?php echo $result['gioitinh'] == 1 ? "Nam" : "Nữ" ?>
                                            </b>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="website">Địa chỉ: </label>
                                            <b>
                                                <?php echo $result['diachi']; ?>
                                            </b>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="eMail">Email: </label>
                                            <b>
                                                <?php echo $result['email']; ?>
                                            </b>
                                        </div>
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="phone">Số điện thoại: </label>
                                            <b>
                                                <?php echo $result['sodt']; ?>
                                            </b>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="phone">Ngày bắt đầu:</label>
                                            <b>
                                                <?php echo $result['ngaybatdau']; ?>
                                            </b>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="phone">Tên đăng nhập: </label>
                                            <b>
                                                <?php echo $result['username']; ?>
                                            </b>
                                        </div>
                                    </div>


                                </div>

                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="text-center">
                                            <div class="form-group">
                                                <div class="col-md-offset-2 col-md-10">
                                                    <p>
                                                        <a href="nhanvienEdit.php?nvId=<?= $result['Id'] ?>" class="btn btn-primary">Cập nhật </a>
                                                        <a class="btn btn-cyan" href="index.php">
                                                            Trở về
                                                        </a>
                                                    </p>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                    </div>

                <?php } ?>
                </div>
            </div>
        </div>
        <!-- footer -->
    </div>
    </div>
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="./bootstrap/assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="./bootstrap/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="./bootstrap/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="./bootstrap/assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="./bootstrap/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="./bootstrap/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="./bootstrap/dist/js/custom.min.js"></script>
    <!-- This Page JS -->
    <script src="./bootstrap/assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <script src="./bootstrap/dist/js/pages/mask/mask.init.js"></script>

    <script src="./bootstrap/assets/libs/quill/dist/quill.min.js"></script>

</body>

</html>