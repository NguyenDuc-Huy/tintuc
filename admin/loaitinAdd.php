<?php

include('../classes/theloai_class.php');
$tl = new theloai();

include_once('../classes/loaitin_class.php');
$loaitin = new loaitin();

session_start();
if (isset($_GET['theloaiId']))
    $id = $_GET['theloaiId'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $tenloaitin = $_POST['tenloaitin'];
    $trangthai = $_POST['trangthai'];
    $loaitin->insertLT($_POST);
}
?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Thêm loại tin</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./bootstrap/assets/images/favicon.png" />
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/select2/dist/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/jquery-minicolors/jquery.minicolors.css" />
    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/quill/dist/quill.snow.css" />
    <link href="./bootstrap/dist/css/style.min.css" rel="stylesheet" />

</head>

<body>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">

        <?php include "./inc/header.php"; ?>

        <?php include "./inc/sidebar.php"; ?>

        <div class="page-wrapper">

            <div class="row">
                <div class="col-md-12">
                    <form action="" method="POST">
                        <div class="card" style=" min-height: 86vh;">
                            <h3 class="card-title active text-center mt-3">THÊM LOẠI TIN MỚI - THỂ LOẠI <?php $tenTL = $tl->getTL($id);
                                                                                                        $res = $tenTL->fetch_assoc();
                                                                                                        echo '<i style= "color:red; font-style:uppercase;">' . $res['tentheloai'] . '</i>';
                                                                                                        ?> </h3>
                            <?php if (isset($_SESSION['response'])) { ?>

                                <div class="alert alert-<?= $_SESSION['res_type']; ?> alert-dismissible fade show mx-2" role="alert">
                                    <strong><?= $_SESSION['response']; ?></strong>
                                    <button type="button" class="btn-close py-3" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            <?php
                                unset($_SESSION['response']);
                                unset($_SESSION['res_type']);
                            } ?>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-end control-label col-form-label">Tên loại tin</label>
                                    <div class="col-sm-5">
                                        <input type="text" name="tenloaitin" value="<?php if (isset($tenloaitin)) echo $tenloaitin; ?>" class="form-control" id="fname" placeholder="Nhập tên loại tin">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 mt-2 text-end ">Hình thức</label>
                                    <div class="col-sm-5">
                                        <select class="select2 form-select shadow-none" name="trangthai" style="width: 100%; height: 36px">
                                            <optgroup label="--Chọn--">
                                                <option value="1" <?php if (isset($trangthai) and $trangthai == 1) echo "selected"; ?>>Hiện</option>
                                                <option value="0" <?php if (isset($trangthai) and $trangthai == 0) echo "selected"; ?>>Ẩn</option>

                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                                <input type="text" hidden name="theloaiId" value="<?php echo $id; ?>">
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-end control-label col-form-label"></label>
                                    <div class="col-sm-5">

                                        <input type="submit" class="btn btn-primary" name="submit" value="Thêm mới">
                                        <a class="btn btn-cyan" href="loaitin.php?theloaiId=<?= $id ?>">
                                            Trở về
                                        </a>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <!-- footer -->
        </div>

    </div>




    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="./bootstrap/assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="./bootstrap/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="./bootstrap/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="./bootstrap/assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="./bootstrap/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="./bootstrap/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="./bootstrap/dist/js/custom.min.js"></script>
    <!-- This Page JS -->
    <script src="./bootstrap/assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <script src="./bootstrap/dist/js/pages/mask/mask.init.js"></script>

    <script src="./bootstrap/assets/libs/quill/dist/quill.min.js"></script>

</body>

</html>