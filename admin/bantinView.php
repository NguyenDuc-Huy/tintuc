<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Xem bản tin</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./bootstrap/assets/images/favicon.png" />
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/select2/dist/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/jquery-minicolors/jquery.minicolors.css" />
    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/quill/dist/quill.snow.css" />
    <link href="./bootstrap/dist/css/style.min.css" rel="stylesheet" />
</head>

<?php
session_start();
include('../classes/bantin_class.php');
$bt = new bantin();

if (!isset($_GET['tinId']) || $_GET['tinId'] == NULL) {
    echo "<script>window.location= 'index.php?module=bantin' </script>";
} else {
    $id = $_GET['tinId'];
}
?>

<body>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">

        <?php include "./inc/header.php"; ?>

        <?php include "./inc/sidebar.php"; ?>

        <div class="page-wrapper">

            <div class="row">
                <div class="col-md-12">
                    <?php
                    $currTin = $bt->getTin($id);
                    if ($currTin) {
                        while ($row = $currTin->fetch_assoc()) {
                    ?>
                            <div class="card" style=" min-height: 86vh;">
                                <p class="mt-3 ml-lg-1">
                                    <a href="javascript:window.history.back(-1);" class="p-2">Trở về</a>
                                    <a href="bantinEdit.php?tinId=<?= $id ?>" class="badge rounded bg-cyan p-2">Sửa</a>

                                </p>
                                <h2 class="card-title active text-center mt-3"><?php echo $row['tieude'] ?></h2>
                                <div class="card-body">
                                    <?php
                                    echo $row['tomtat'];
                                    if ($row['noidung'] != "" and $row['noidung'] != NULL) echo $row['noidung'];
                                    if ($row['linkvideo'] != "" and $row['linkvideo'] != NULL) echo $row['linkvideo'];
                                    ?>
                                </div>

                            </div>
                    <?php }
                    } ?>
                </div>
            </div>
            <?php include './inc/footer.php' ?>
        </div>

    </div>




    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="./bootstrap/assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="./bootstrap/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="./bootstrap/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="./bootstrap/assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="./bootstrap/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="./bootstrap/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="./bootstrap/dist/js/custom.min.js"></script>
    <!-- This Page JS -->
    <script src="./bootstrap/assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <script src="./bootstrap/dist/js/pages/mask/mask.init.js"></script>
    <script src="./bootstrap/assets/libs/select2/dist/js/select2.full.min.js"></script>
    <script src="./bootstrap/assets/libs/select2/dist/js/select2.min.js"></script>
    <script src="./bootstrap/assets/libs/jquery-asColor/dist/jquery-asColor.min.js"></script>
    <script src="./bootstrap/assets/libs/jquery-asGradient/dist/jquery-asGradient.js"></script>
    <script src="./bootstrap/assets/libs/jquery-asColorPicker/dist/jquery-asColorPicker.min.js"></script>
    <script src="./bootstrap/assets/libs/jquery-minicolors/jquery.minicolors.min.js"></script>
    <script src="./bootstrap/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="./bootstrap/assets/libs/quill/dist/quill.min.js"></script>


</body>

</html>