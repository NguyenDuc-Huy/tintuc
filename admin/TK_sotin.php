<script src="https://code.highcharts.com/highcharts.js"></script>
<div class="card h-100">
    <div class="card-body">
        <h3 class="card-title text-center text-uppercase">Số lượng bài viết từng thể loại</h3>

        <div class="container">
            <div id="container" style="width: 100%; height: 400px;"></div>
        </div>
        <script>
            $(function() {
                $.post('dataSoTin.php', {}, function(data) {
                    var data = jQuery.parseJSON(data);
                    console.log(data);
                    Highcharts.chart('container', {
                        title: {
                            text: ''
                        },
                        xAxis: {
                            categories: data[0]
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Số tin'
                            }
                        },
                        series: [{
                            name: 'Số tin',
                            data: data[1],
                            type: 'column'
                        }]
                    });
                });

            });
        </script>

    </div>
</div>
<!-- footer -->