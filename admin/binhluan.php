<?php
include('../classes/binhluan_class.php');
$binhluan = new binhluan();
if (isset($_GET['delId'])) {

    $id = $_GET['delId'];
    $binhluan->deleteBL($id);
}
if (isset($_GET['editId']) and isset($_GET['value'])) {
    $id = $_GET['editId'];
    $value = $_GET['value'];
    $binhluan->updateBL($id, $value);
} ?>
<div class="card">
    <div class="card-body">
        <h3 class="card-title active text-center">DANH SÁCH CÁC BÌNH LUẬN</h3>
        <?php if (isset($_SESSION['response'])) { ?>

            <div class="alert alert-<?= $_SESSION['res_type']; ?> alert-dismissible fade show mx-2" role="alert">
                <strong><?= $_SESSION['response']; ?></strong>
                <button type="button" class="btn-close py-3" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php
            unset($_SESSION['response']);
            unset($_SESSION['res_type']);
        } ?>

        <div class="table-responsive">

            <table id="zero_config" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="font-weight: bold;">STT</th>
                        <th style="font-weight: bold;">Mã bình luận</th>
                        <th style="font-weight: bold;">Tên người bình luận</th>
                        <th style="font-weight: bold;">Mã bài viết</th>
                        <th style="font-weight: bold;">Nội dung</th>
                        <th style="font-weight: bold;">Ngày bình luận</th>
                        <th style="font-weight: bold;">Ngày cập nhật</th>
                        <th style="font-weight: bold;">Trạng thái</th>
                        <th style="font-weight: bold;">Ẩn/hiện</th>
                        <th style="font-weight: bold;">Hành động</th>


                    </tr>
                </thead>
                <tbody>

                    <?php
                    include '../classes/docgia_class.php';

                    $user = new docgia();

                    $list = $binhluan->showBL();

                    if ($list) {
                        $i = 0;
                        while ($result = $list->fetch_assoc()) {
                            $i++;

                    ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td> <?php echo $result['binhluanId'] ?></td>

                                <?php
                                $dg = $user->getDG($result['userId']);
                                if ($dg) {
                                    $tendg = $dg->fetch_assoc();
                                    echo ' <td>' . $tendg['name'] . ' </td>';
                                }

                                ?>


                                <td> <?php echo $result['tinId'] ?></td>
                                <td> <?php echo $result['noidung'] ?></td>
                                <td> <?php echo $result['ngaytao'] ?></td>
                                <td> <?php echo $result['ngaycapnhat'] ?></td>
                                <?php echo $result['trangthai'] == 1 ? "<td>Hiển thị</td>" : "<td>Ẩn</td>" ?>
                                <td>
                                    <?php if ($result['trangthai'] == 1) { ?>
                                        <a href="?module=binhluan&editId=<?= $result['binhluanId'] ?>&value=0" class="badge rounded bg-cyan p-2">Ẩn bình luận</a>
                                    <?php } else { ?>
                                        <a href="?module=binhluan&editId=<?= $result['binhluanId'] ?>&value=1" class="badge rounded bg-primary p-2">Hiện bình luận</a>
                                    <?php } ?>
                                </td>
                                <td>


                                    <a href="?module=binhluan&delId=<?= $result['binhluanId'] ?>" class="badge rounded bg-danger p-2" onclick="return confirm('Do you want delete this record?');">Xóa</a>
                                </td>
                            </tr>
                    <?php  }
                    }
                    ?>

                </tbody>

            </table>
        </div>
    </div>
</div>