<?php
session_start();
if (!isset($_SESSION['userId'])) {
    header("Location: ../login.php");
}

?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <meta name="robots" content="noindex,nofollow" />
    <title>Admin Control</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./bootstrap/assets/images/favicon.png" />

    <link href="./bootstrap/assets/libs/flot/css/float-chart.css" rel="stylesheet" />
    <!-- Custom CSS calendar-->
    <link href="./bootstrap/assets/libs/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" />
    <link href="./bootstrap/assets/extra-libs/calendar/calendar.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="./bootstrap/dist/css/style.min.css" rel="stylesheet" />

    <!-- CSS for table -->
    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/extra-libs/multicheck/multicheck.css" />
    <link href="./bootstrap/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet" />

</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <!-- <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div> -->
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include "./inc/header.php"; ?>

        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include "./inc/sidebar.php"; ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="ms-auto text-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        Library
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <?php
            if (isset($_GET["module"])) {
                $module = $_GET["module"];
                include($module . ".php");
            } else include("./home.php");
            ?>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php include("./inc/footer.php"); ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="./bootstrap/assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="./bootstrap/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="./bootstrap/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="./bootstrap/assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="./bootstrap/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="./bootstrap/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="./bootstrap/dist/js/custom.min.js"></script>
    <!--This page JavaScript -->
    <!-- <script src="./bootstrap/dist/js/pages/dashboards/dashboard1.js"></script> -->
    <!-- Charts js Files -->
    <script src="./bootstrap/assets/libs/flot/excanvas.js"></script>
    <script src="./bootstrap/assets/libs/flot/jquery.flot.js"></script>
    <script src="./bootstrap/assets/libs/flot/jquery.flot.pie.js"></script>
    <script src="./bootstrap/assets/libs/flot/jquery.flot.time.js"></script>
    <script src="./bootstrap/assets/libs/flot/jquery.flot.stack.js"></script>
    <script src="./bootstrap/assets/libs/flot/jquery.flot.crosshair.js"></script>
    <script src="./bootstrap/assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="./bootstrap/dist/js/pages/chart/chart-page-init.js"></script>


    <!-- this page js -->
    <script src="./bootstrap/assets/extra-libs/multicheck/datatable-checkbox-init.js"></script>
    <script src="./bootstrap/assets/extra-libs/multicheck/jquery.multicheck.js"></script>
    <script src="./bootstrap/assets/extra-libs/DataTables/datatables.min.js"></script>
    <script>
        $("#zero_config").DataTable();
    </script>
    <script src="./bootstrap/dist/js/jquery.ui.touch-punch-improved.js"></script>
    <script src="./bootstrap/dist/js/jquery-ui.min.js"></script>
    <script src="./bootstrap/assets/libs/moment/min/moment.min.js"></script>
    <script src="./bootstrap/assets/libs/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="./bootstrap/dist/js/pages/calendar/cal-init.js"></script>
    <!-- modal confirm -->

</body>

</html>