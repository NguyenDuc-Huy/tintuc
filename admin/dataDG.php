<?php
include_once('../classes/docgia_class.php');

$dg =  new docgia();
$thang =  [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
];
foreach ($thang as $key => $value) {
    $sodg = $dg->soDGThang($key + 1);
    if ($sodg) {
        $res1 = $sodg->fetch_assoc();
        $index[] = (int) $res1['SDG'];
    }
}

$x = [$thang, $index];
echo json_encode($x);
