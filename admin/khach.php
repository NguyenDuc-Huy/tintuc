<?php

include_once('../classes/docgia_class.php');

$khach = new docgia();

if (isset($_GET['delId'])) {
    $delId = $_GET['delId'];
    $khach->deleteDG($delId);
}

if (isset($_GET['editId']) and isset($_GET['value'])) {
    $id = $_GET['editId'];
    $value = $_GET['value'];
    $khach->updateStatus($id, $value);
}
?>


<div class="card h-100">
    <div class="card-body">
        <h3 class="card-title text-center text-uppercase">Danh sách các độc giả</h3>
        <?php if (isset($_SESSION['response'])) { ?>

            <div class="alert alert-<?= $_SESSION['res_type']; ?> alert-dismissible fade show mx-2" role="alert">
                <strong><?= $_SESSION['response']; ?></strong>
                <button type="button" class="btn-close py-3" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php
            unset($_SESSION['response']);
            unset($_SESSION['res_type']);
        } ?>


        <div class="table-responsive">
            <table id="zero_config" class="table table-striped table-bordered overflow-y-auto">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Họ tên độc giả</th>
                        <th>Giới tính</th>
                        <th>Email</th>
                        <th>Ngày đăng ký</th>
                        <th>Trạng thái</th>
                        <th>Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $list = $khach->showDG();
                    if ($list) {
                        $i = 0;
                        while ($result = $list->fetch_assoc()) {
                            $i++;
                    ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $result['name'] ?> </td>
                                <?php echo $result['gioitinh'] == 1 ? "<td>Nam</td>" : "<td>Nữ</td>"; ?>
                                <td> <?php echo $result['email'] ?></td>

                                <td> <?php echo $result['ngaydk'] ?></td>
                                <td> <?php echo $result['active'] == 1 ? "Active" : "Inactive" ?></td>



                                <td> <?php if ($result['active'] == 1) { ?>
                                        <a href="?module=khach&editId=<?= $result['userId'] ?>&value=0" class="badge rounded bg-primary p-2">Inactive</a>
                                    <?php } else { ?>
                                        <a href="?module=khach&editId=<?= $result['userId'] ?>&value=1" class="badge rounded bg-cyan p-2">Active</a>
                                    <?php } ?>
                                    <a class="badge rounded bg-success p-2" href="khachView.php?dgId=<?= $result['userId'] ?>?>">Xem</a>
                                    <a href="?module=khach&delId=<?= $result['userId'] ?>" class="badge rounded bg-danger p-2" onclick="return confirm('Do you want delete this record?');">Xóa</a>
                                </td>
                            </tr>
                    <?php }
                    } ?>
                </tbody>
            </table>

        </div>
    </div>
</div>
<!-- footer -->