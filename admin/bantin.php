<?php
include('../classes/bantin_class.php');
include('../classes/theloai_class.php');
include('../classes/loaitin_class.php');
$bantin = new bantin();
$theloai = new theloai();
$loaitin = new loaitin();

if (isset($_GET['delId'])) {

    $id = $_GET['delId'];
    $bantin->deleteTin($id);
} ?>
<div class="card">
    <div class="card-body">
        <h3 class="card-title active text-center">DANH SÁCH CÁC BẢN TIN</h3>
        <?php if (isset($_SESSION['response'])) { ?>

            <div class="alert alert-<?= $_SESSION['res_type']; ?> alert-dismissible fade show mx-2" role="alert">
                <strong><?= $_SESSION['response']; ?></strong>
                <button type="button" class="btn-close py-3" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php
            unset($_SESSION['response']);
            unset($_SESSION['res_type']);
        } ?>
        <p> <a href="bantinAdd.php" class="p-2" style="font-size: medium;">Thêm mới</a></p>
        <div class="table-responsive">

            <table id="zero_config" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="font-weight: bold;">STT</th>
                        <th style="font-weight: bold;">Mã tin</th>
                        <th style="font-weight: bold;">Tiêu đề</th>
                        <th style="font-weight: bold;">Tóm tắt</th>
                        <th style="font-weight: bold;">Video</th>
                        <th style="font-weight: bold;">Nổi bật</th>
                        <th style="font-weight: bold;">Số lượt xem</th>
                        <th style="font-weight: bold;">Trạng thái</th>
                        <th style="font-weight: bold;">Ngày tạo</th>
                        <th style="font-weight: bold;">Ngày cập nhật</th>
                        <th style="font-weight: bold;">Thể loại</th>
                        <th style="font-weight: bold;">Loại tin</th>

                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $list = $bantin->showTin();
                    if ($list) {
                        $i = 0;
                        while ($result = $list->fetch_assoc()) {
                            $i++;
                    ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td> <?php echo $result['tinId'] ?></td>
                                <td><?php echo $result['tieude'];
                                    echo '<img src="./upload/images/' . $result['hinh'] . '" alt="Ảnh bản tin" width="100" height="100">';
                                    ?> </td>
                                <td><?php echo $bantin->textShorten($result['tomtat'], 50) ?> </td>
                                <?php echo $result['linkvideo'] != NULL ? "<td>Có</td>" : "<td>Không</td>" ?>
                                <?php echo $result['noibat'] == 1 ? "<td>Có</td>" : "<td>Không</td>" ?>
                                <td><?php echo $result['soluotxem'] ?> </td>
                                <?php echo $result['trangthai'] == 1 ? "<td>Xuất bản</td>" : "<td>Ẩn</td>" ?>
                                <td><?php echo $result['ngaytao'] ?> </td>
                                <td><?php echo $result['ngaycapnhat'] ?></td>

                                <?php
                                $tl = $theloai->getTL($result['theloaiId']);
                                $tentl = $tl->fetch_assoc();
                                ?>
                                <td><?php echo $tentl['tentheloai']; ?></td>

                                <?php
                                $lt = $loaitin->getLT($result['loaitinId']);
                                $tenlt = $lt->fetch_assoc();
                                ?>
                                <td><?php echo $tenlt['tenloaitin']; ?></td>


                                <td>
                                    <a href="bantinView.php?tinId=<?= $result['tinId'] ?>" class="badge rounded bg-secondary p-2">Xem</a>
                                </td>
                                <td>
                                    <a href="bantinEdit.php?tinId=<?= $result['tinId'] ?>" class="badge rounded bg-cyan p-2">Sửa</a>
                                </td>
                                <td>
                                    <a href="?module=bantin&delId=<?= $result['tinId'] ?>" class="badge rounded bg-danger p-2" onclick="return confirm('Do you want delete this record?');">Xóa</a>
                                </td>
                            </tr>
                    <?php  }
                    }
                    ?>

                </tbody>

            </table>
        </div>
    </div>
</div>