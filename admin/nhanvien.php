<?php

include_once('../classes/nhanvien_class.php');

$nv = new nhanvien();

if (isset($_GET['delId'])) {
    $delId = $_GET['delId'];
    $nv->deleteNV($delId);
}
?>


<div class="card h-100">
    <div class="card-body">
        <h3 class="card-title text-center text-uppercase">Danh sách nhân viên</h3>
        <?php if (isset($_SESSION['response'])) { ?>

            <div class="alert alert-<?= $_SESSION['res_type']; ?> alert-dismissible fade show mx-2" role="alert">
                <strong><?= $_SESSION['response']; ?></strong>
                <button type="button" class="btn-close py-3" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php
            unset($_SESSION['response']);
            unset($_SESSION['res_type']);
        } ?>
        <p>
            <a href="nhanvienAdd.php" class="p-2 mb-2" style="font-size: medium;">Thêm mới</a>
        </p>

        <div class="table-responsive">
            <table id="zero_config" class="table table-striped table-bordered overflow-y-auto">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Họ tên nhân viên</th>
                        <th>Giới tính</th>
                        <th>Địa chỉ</th>

                        <th>Ngày bắt đầu</th>
                        <th>Chức vụ</th>
                        <th>Username</th>

                        <th>Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $list = $nv->showNV();
                    if ($list) {
                        $i = 0;
                        while ($result = $list->fetch_assoc()) {
                            $i++;
                    ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $result['hotenNV'] ?> </td>
                                <?php echo $result['gioitinh'] == 1 ? "<td>Nam</td>" : "<td>Nữ</td>"; ?>
                                <td> <?php echo $result['diachi'] ?></td>

                                <td> <?php echo $result['ngaybatdau'] ?></td>
                                <?php echo $result['chucvu'] == 1 ? "<td>Admin</td>" : "<td>Nhân viên</td>"; ?>
                                <td> <?php echo $result['username'] ?></td>

                                <td><a class="badge rounded bg-success p-2" href="nhanvienView.php?nvId=<?= $result['Id'] ?>?>">Xem</a>
                                    <a href="?module=nhanvien&delId=<?= $result['Id'] ?>" class="badge rounded bg-danger p-2" onclick="return confirm('Do you want delete this record?');">Xóa</a>
                                    <a class="badge rounded bg-cyan p-2" href="nhanvienEdit.php?nvId=<?= $result['Id'] ?>">Sửa</a>
                                </td>
                            </tr>
                    <?php }
                    } ?>
                </tbody>
            </table>

        </div>
    </div>
</div>
<!-- footer -->