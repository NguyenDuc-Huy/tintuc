<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Loại tin</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./bootstrap/assets/images/favicon.png" />
    <!-- Custom CSS -->

    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/quill/dist/quill.snow.css" />
    <link href="./bootstrap/dist/css/style.min.css" rel="stylesheet" />

    <link href="./bootstrap/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet" />

</head>

<?php
include_once('../classes/theloai_class.php');
include_once('../classes/loaitin_class.php');
session_start();
$theloai = new theloai();
$loaitin = new loaitin();

if (!isset($_GET['theloaiId']) || $_GET['theloaiId'] == NULL) {
    echo "<script>window.location= 'index.php?module=theloai' </script>";
} else {
    $id = $_GET['theloaiId'];
}

if (isset($_GET['delId'])) {

    $delId = $_GET['delId'];
    $loaitin->deleteLT($delId);
}
?>

<body>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">

        <?php include "./inc/header.php"; ?>

        <?php include "./inc/sidebar.php"; ?>

        <div class="page-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <div class="card h-100">
                        <div class="card-body">
                            <h3 class="card-title text-center text-uppercase">Danh sách loại tin <?php $tenTL = $theloai->getTL($id);
                                                                                                    $res = $tenTL->fetch_assoc();
                                                                                                    echo '<i style= "color:red;font-style:uppercase;">' . $res['tentheloai'] . '</i>';
                                                                                                    ?></h3>
                            <?php if (isset($_SESSION['response'])) { ?>

                                <div class="alert alert-<?= $_SESSION['res_type']; ?> alert-dismissible fade show mx-2" role="alert">
                                    <strong><?= $_SESSION['response']; ?></strong>
                                    <button type="button" class="btn-close py-3" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            <?php
                                unset($_SESSION['response']);
                                unset($_SESSION['res_type']);
                            } ?>
                            <p> <a href="loaitinAdd.php?theloaiId=<?= $id ?>" class="p-2 mb-2" style="font-size: medium;">Thêm mới</a>
                                <a href="index.php?module=theloai" class="p-2 mb-2 text-danger" style="font-size: medium;">Quay lại</a>
                            </p>

                            <div class="table-responsive">
                                <table id="zero_config" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Mã loại tin</th>
                                            <th>Tên loại tin</th>
                                            <th>Trạng thái</th>
                                            <th>Hành động</th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $list = $loaitin->getLTTL($id);
                                        if ($list) {
                                            $i = 0;
                                            while ($result = $list->fetch_assoc()) {
                                                $i++;
                                        ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td> <?php echo $result['loaitinId'] ?></td>
                                                    <td><?php echo $result['tenloaitin'] ?> </td>
                                                    <?php echo $result['trangthai'] == 1 ? "<td>Hiển thị</td>" : "<td>Ẩn</td>" ?>
                                                    <td>
                                                        <a href="loaitin.php?theloaiId=<?= $id ?>&delId=<?= $result['loaitinId'] ?>" class="badge rounded bg-danger p-2" onclick="return confirm('Do you want delete this record?');">Xóa</a> |
                                                        <a class="badge rounded bg-cyan p-2" href="loaitinEdit.php?theloaiId=<?= $id ?>&loaitinId=<?= $result['loaitinId'] ?>">Sửa</a>
                                                    </td>
                                                </tr>
                                        <?php }
                                        } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <!-- footer -->
                </div>

            </div>
        </div>
        <!-- Button trigger modal -->

    </div>

    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="./bootstrap/assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="./bootstrap/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="./bootstrap/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="./bootstrap/assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="./bootstrap/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="./bootstrap/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="./bootstrap/dist/js/custom.min.js"></script>
    <!-- This Page JS -->
    <script src="./bootstrap/assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <script src="./bootstrap/dist/js/pages/mask/mask.init.js"></script>
    <script src="./bootstrap/assets/extra-libs/DataTables/datatables.min.js"></script>
    <script>
        $("#zero_config").DataTable();
    </script>
    <script src="./bootstrap/assets/libs/quill/dist/quill.min.js"></script>



</body>

</html>