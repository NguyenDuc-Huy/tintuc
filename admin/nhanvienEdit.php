<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Thêm nhân viên</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./bootstrap/assets/images/favicon.png" />
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/select2/dist/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/jquery-minicolors/jquery.minicolors.css" />
    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/quill/dist/quill.snow.css" />
    <link href="./bootstrap/dist/css/style.min.css" rel="stylesheet" />

</head>

<?php
include_once('../classes/nhanvien_class.php');
session_start();
$nv = new nhanvien();
if (!isset($_GET['nvId']) || $_GET['nvId'] == NULL) {
    echo "<script>window.location= 'index.php?module=nhanvien' </script>";
} else {
    $id = $_GET['nvId'];
}

if (isset($_POST['submit'])) {
    $nv->updateNV($_POST, $_FILES['image'], $id);
}
?>

<body>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">

        <?php include "./inc/header.php"; ?>

        <?php include "./inc/sidebar.php"; ?>

        <div class="page-wrapper">
            <div class="card" style=" min-height: 86vh;">
                <?php if (isset($_SESSION['response'])) { ?>

                    <div class="alert alert-<?= $_SESSION['res_type']; ?> alert-dismissible fade show mx-2" role="alert">
                        <strong><?= $_SESSION['response']; ?></strong>
                        <button type="button" class="btn-close py-3" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                <?php
                    unset($_SESSION['response']);
                    unset($_SESSION['res_type']);
                } ?>
                <?php
                $result = $nv->getNV($id);
                if ($result) {
                    $row = $result->fetch_assoc();
                ?>
                    <form action="" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-8">
                                <h3 class="card-title active text-center mt-3">CẬP NHẬT THÔNG TIN NHÂN VIÊN</h3>


                                <div class=" card-body">
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-end control-label col-form-label">Họ tên nhân viên</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="fname" name="hoten" value="<?php echo $row['hotenNV']; ?>" placeholder="Nhập họ tên nhân viên">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 mt-2 text-end ">Giới tính</label>
                                        <div class="col-sm-8 mt-2 ">
                                            <input type="radio" class="form-check-input" name="gioitinh" <?php if ($row['gioitinh'] == 1) echo "checked"; ?> value="1" />
                                            <label class="form-check-label mb-0">Nam</label> &nbsp;&nbsp;
                                            <input type="radio" class="form-check-input" name="gioitinh" <?php if ($row['gioitinh'] == 0) echo "checked"; ?> value="0" />
                                            <label class="form-check-label mb-0">Nữ</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-end control-label col-form-label">Địa chỉ</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="fname" name="diachi" value="<?php echo $row['diachi']; ?>" placeholder="Nhập địa chỉ">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-end control-label col-form-label">Số điện thoại</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="fname" name="sodt" value="<?php echo $row['sodt']; ?>" placeholder="Nhập số điện thoại">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-end control-label col-form-label">Ngày bắt đầu</label>
                                        <div class="col-sm-8">
                                            <input type="date" class="form-control" id="fname" name="ngaybatdau" value="<?php echo $row['ngaybatdau'];  ?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-end control-label col-form-label">Email</label>
                                        <div class="col-sm-8">
                                            <input type="email" class="form-control" id="fname" name="email" value="<?php echo $row['email']; ?>" placeholder="Nhập email">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 mt-2 text-end ">Chức vụ</label>
                                        <div class="col-sm-8 mt-2 ">
                                            <input type="radio" class="form-check-input" name="chucvu" <?php if ($row['chucvu'] == 1) echo "checked"; ?> value="1" />
                                            <label class="form-check-label mb-0">Admin</label> &nbsp;&nbsp;
                                            <input type="radio" class="form-check-input" name="chucvu" <?php if ($row['chucvu'] == 0) echo "checked"; ?> value="0" />
                                            <label class="form-check-label mb-0">Nhân viên</label>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control" id="fname" name="hinh" hidden value="<?php echo $row['anhdaidien']; ?>">
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-end control-label col-form-label"></label>
                                        <div class="col-sm-8">
                                            <input type="submit" class="btn btn-primary" name="submit" value="Cập nhật">
                                            <a class="btn btn-cyan" href="index.php?module=nhanvien">
                                                Trở về
                                            </a>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="col-md-3">
                                <div class="form-group text-center p-2 mt-lg-5 rounded" style="outline: dashed 1px gray;">
                                    <label for="fname" class="control-label col-form-label">Ảnh đại diện</label>
                                    <img width="200" <?php echo $row['anhdaidien'] != NULL ? 'src="./upload/avatar/' . $row['anhdaidien'] . '"' : 'src="./css/images/avatar.jpg"' ?> alt="anhdaidien">
                                    <div class="">
                                        <input type="file" class="form-control" id="fname" name="image">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>

                <?php } ?>
            </div>
            <!-- footer -->
        </div>

    </div>

    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="./bootstrap/assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="./bootstrap/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="./bootstrap/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="./bootstrap/assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="./bootstrap/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="./bootstrap/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="./bootstrap/dist/js/custom.min.js"></script>
    <!-- This Page JS -->
    <script src="./bootstrap/assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>


</body>

</html>