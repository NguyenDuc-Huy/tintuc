<?php
include('../classes/theloai_class.php');
include('../classes/bantin_class.php');
$theloai = new theloai();

if (isset($_GET['delId'])) {

    $id = $_GET['delId'];
    $theloai->deleteTL($id);
} ?>
<div class="card">
    <div class="card-body">
        <h3 class="card-title active text-center">DANH SÁCH THỂ LOẠI TIN</h3>
        <?php if (isset($_SESSION['response'])) { ?>

            <div class="alert alert-<?= $_SESSION['res_type']; ?> alert-dismissible fade show mx-2" role="alert">
                <strong><?= $_SESSION['response']; ?></strong>
                <button type="button" class="btn-close py-3" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php
            unset($_SESSION['response']);
            unset($_SESSION['res_type']);
        } ?>
        <p> <a href="theloaiAdd.php" class="p-2" style="font-size: medium;">Thêm mới</a></p>
        <div class="table-responsive">

            <table id="zero_config" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="font-weight: bold;">STT</th>
                        <th style="font-weight: bold;">Mã thể loại</th>
                        <th style="font-weight: bold;">Tên thể loại</th>
                        <th style="font-weight: bold;">Trạng thái</th>
                        <th style="font-weight: bold;">Hành động</th>
                    </tr>
                </thead>
                <tbody>

                    <?php


                    $list = $theloai->showTL();
                    if ($list) {
                        $i = 0;
                        while ($result = $list->fetch_assoc()) {

                            $i++;
                    ?>
                            <tr>
                                <td><?php echo $i;

                                    ?></td>
                                <td> <?php echo $result['theloaiId'] ?></td>
                                <td><?php echo $result['tentheloai'] ?> </td>
                                <?php echo $result['anhien'] == 1 ? "<td>Hiển thị</td>" : "<td>Ẩn</td>" ?>
                                <td>
                                    <a href="?module=theloai&delId=<?= $result['theloaiId'] ?>" class="badge rounded bg-danger p-2" onclick="return confirm('Do you want delete this record?');">Xóa</a> |
                                    <a href="theloaiEdit.php?theloaiId=<?= $result['theloaiId'] ?>" class="badge rounded bg-cyan p-2">Sửa</a> |
                                    <a href="loaitin.php?theloaiId=<?= $result['theloaiId'] ?>" class="badge rounded bg-success p-2">Xem loại tin </a>

                                </td>
                            </tr>
                    <?php  }
                    }
                    ?>

                </tbody>

            </table>
        </div>
    </div>
</div>