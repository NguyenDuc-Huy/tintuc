<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Cập nhật bản tin</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./bootstrap/assets/images/favicon.png" />
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/select2/dist/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/jquery-minicolors/jquery.minicolors.css" />
    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="./bootstrap/assets/libs/quill/dist/quill.snow.css" />
    <link href="./bootstrap/dist/css/style.min.css" rel="stylesheet" />
</head>

<?php
include('../classes/theloai_class.php');
include('../classes/loaitin_class.php');
include('../classes/bantin_class.php');
session_start();
$tl = new theloai();
$lt = new loaitin();
$bt = new bantin();

if (!isset($_GET['tinId']) || $_GET['tinId'] == NULL) {
    echo "<script>window.location= 'index.php?module=bantin' </script>";
} else {
    $id = $_GET['tinId'];
}


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // $tieude = $_POST['tieude'];
    // $tomtat = $_POST['tomtat'];
    // $noidung = $_POST['noidung'];
    // $noibat = $_POST['noibat'];
    // $trangthai = $_POST['trangthai'];
    // $theloaiId = $_POST['theloaiId'];
    // $loaitinId = $_POST['loaitinId'];
    $bt->updateTin($_POST, $_FILES['image'], $id);
}
?>

<body>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">

        <?php include "./inc/header.php"; ?>

        <?php include "./inc/sidebar.php"; ?>

        <div class="page-wrapper">

            <div class="row">
                <div class="col-md-12">
                    <form action="" method="POST" enctype="multipart/form-data">
                        <div class="card" style=" min-height: 86vh;">
                            <h3 class="card-title active text-center mt-3">CẬP NHẬT BẢN TIN</h3>
                            <?php if (isset($_SESSION['response'])) { ?>

                                <div class="alert alert-<?= $_SESSION['res_type']; ?> alert-dismissible fade show mx-2" role="alert">
                                    <strong><?= $_SESSION['response']; ?></strong>
                                    <button type="button" class="btn-close py-3" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            <?php
                                unset($_SESSION['response']);
                                unset($_SESSION['res_type']);
                            } ?>
                            <div class="card-body">
                                <?php
                                $currTin = $bt->getTin($id);
                                if ($currTin) {
                                    while ($row = $currTin->fetch_assoc()) {

                                ?>
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-1 text-end control-label col-form-label">Tiêu đề</label>
                                            <div class="col-sm-11">
                                                <input type="text" name="tieude" value="<?php echo $row['tieude']; ?>" class="form-control" id="fname" placeholder="Nhập tiêu đề">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-1 text-end control-label col-form-label">Tóm tắt</label>
                                            <div class="col-sm-11">
                                                <textarea class="form-control" name="tomtat" id="ckeditor1"><?php echo $row['tomtat']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-1 text-end control-label col-form-label">Nội dung</label>
                                            <div class="col-sm-11">
                                                <textarea class="form-control" name="noidung" id="ckeditor2"><?php echo $row['noidung']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label for="fname" class="col-sm-1 text-end control-label col-form-label">Video</label>
                                            <div class="col-sm-11">
                                                <?php echo $row['linkvideo']; ?>
                                            </div>
                                        </div>
                                        <div class="form-group row">

                                            <label for="fname" class="col-sm-1 text-end control-label col-form-label">Link nhúng</label>
                                            <div class="col-sm-11">

                                                <textarea class="form-control" name="video"><?php echo $row['linkvideo']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-1 text-end control-label col-form-label">Hình ảnh</label>
                                            <div class="col-sm-4">
                                                <img src="./upload/images/<?php echo $row['hinh']; ?>" width="80" height="80"><br>
                                                <input type="file" name="image" class="form-control" id="fname">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-1 mt-2 text-end ">Hình thức</label>
                                            <div class="col-sm-5 mt-2 ">
                                                <input type="radio" class="form-check-input" name="trangthai" <?php if ($row['trangthai'] == 1) echo 'checked' ?> value="1" />
                                                <label class="form-check-label mb-0">Xuất bản</label> &nbsp;&nbsp;
                                                <input type="radio" class="form-check-input" name="trangthai" <?php if ($row['trangthai'] == 0) echo 'checked' ?> value="0" />
                                                <label class="form-check-label mb-0">Ẩn</label>
                                            </div>
                                            <label for="fname" class="col-sm-1 mt-2 text-end ">Nổi bật</label>
                                            <div class="col-sm-5 mt-2 ">
                                                <input type="radio" class="form-check-input" name="noibat" value="1" <?php if ($row['noibat'] == 1) echo 'checked' ?> />
                                                <label class="form-check-label mb-0">Có</label> &nbsp;&nbsp;
                                                <input type="radio" class="form-check-input" name="noibat" <?php if ($row['noibat'] == 0) echo 'checked' ?> value="0" />
                                                <label class="form-check-label mb-0">Không</label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-1 mt-2 text-end "> Thể loại </label>
                                            <div class="col-sm-4">
                                                <select class="select2 form-select shadow-none" name="theloaiId" id="theloai" style="width: 100%; height: 36px">
                                                    <option value="">---Chọn thể loại---</option>
                                                    <?php
                                                    $list = $tl->showTL();
                                                    if ($list) {
                                                        while ($res = $list->fetch_assoc()) {
                                                            if ($res['theloaiId'] == $row['theloaiId'])
                                                                echo '<option value="' . $res['theloaiId'] . '" selected>' . $res['tentheloai'] . '</option>';
                                                            else
                                                                echo '<option value="' . $res['theloaiId'] . '">' . $res['tentheloai'] . '</option>';
                                                        }
                                                    }

                                                    ?>
                                                </select>
                                            </div>

                                            <label for="fname" class="col-sm-2 mt-2 text-end "> Loại tin </label>
                                            <div class="col-sm-4">
                                                <select class="select2 form-select shadow-none" name="loaitinId" id="loaitin" style="width: 100%; height: 36px">
                                                    <?php
                                                    $list2 = $lt->getLTTL($row['theloaiId']);
                                                    if ($list2) {
                                                        while ($res2 = $list2->fetch_assoc()) {
                                                            if ($res2['loaitinId'] == $row['loaitinId'])
                                                                echo '<option value="' . $res2['loaitinId'] . '"selected>' . $res2['tenloaitin'] . '</option>';
                                                            else
                                                                echo '<option value="' . $res2['loaitinId'] . '">' . $res2['tenloaitin'] . '</option>';
                                                        }
                                                    }

                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <input type="text" name="hinh" value="<?php echo $row['hinh']; ?>" hidden>
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-1 text-end control-label col-form-label"></label>
                                            <div class="col-sm-4">
                                                <input type="submit" class="btn btn-primary" name="submit" value="Cập nhật">
                                                <a class="btn btn-cyan" href="index.php?module=bantin">
                                                    Trở về
                                                </a>
                                            </div>
                                        </div>
                                <?php  }
                                } ?>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <?php include './inc/footer.php' ?>
        </div>

    </div>




    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="./bootstrap/assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="./bootstrap/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="./bootstrap/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="./bootstrap/assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="./bootstrap/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="./bootstrap/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="./bootstrap/dist/js/custom.min.js"></script>
    <!-- This Page JS -->
    <script src="./bootstrap/assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <script src="./bootstrap/dist/js/pages/mask/mask.init.js"></script>
    <script src="./bootstrap/assets/libs/select2/dist/js/select2.full.min.js"></script>
    <script src="./bootstrap/assets/libs/select2/dist/js/select2.min.js"></script>
    <script src="./bootstrap/assets/libs/jquery-asColor/dist/jquery-asColor.min.js"></script>
    <script src="./bootstrap/assets/libs/jquery-asGradient/dist/jquery-asGradient.js"></script>
    <script src="./bootstrap/assets/libs/jquery-asColorPicker/dist/jquery-asColorPicker.min.js"></script>
    <script src="./bootstrap/assets/libs/jquery-minicolors/jquery.minicolors.min.js"></script>
    <script src="./bootstrap/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="./bootstrap/assets/libs/quill/dist/quill.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="./source/ckeditor/ckeditor.js"></script>
    <script src="./source/ckfinder/ckfinder.js"></script>
    <script>
        CKEDITOR.replace("ckeditor1", {
            filebrowserBrowseUrl: '/tintuc/admin/source/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: '/tintuc/admin/source/ckfinder/ckfinder.html?type=Images',
            filebrowserImageUploadUrl: '/tintuc/admin/source/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserUploadUrl: '/tintuc/admin/source/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
        });
    </script>
    <script>
        CKEDITOR.replace("ckeditor2", {
            filebrowserBrowseUrl: '/tintuc/admin/source/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: '/tintuc/admin/source/ckfinder/ckfinder.html?type=Images',
            filebrowserImageUploadUrl: '/tintuc/admin/source/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserUploadUrl: '/tintuc/admin/source/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
        });
    </script>

    <script>
        // $(document).ready(function() {
        //     $("#theloai").change(function() {
        //         var theloai = $(this).val();
        //         $.post("./get_loaitin.php", function(data) {
        //             $("#loaitin").html(data);
        //         });
        //     });
        // });


        $(document).ready(function() {
            $('#theloai').on('change', function() {
                var theloaiId = this.value;
                $.ajax({
                    url: "get_loaitin.php",
                    type: "POST",
                    data: {
                        theloaiId: theloaiId
                    },
                    cache: false,
                    success: function(result) {
                        $("#loaitin").html(result);
                    }
                });
            });
        });
    </script>

</body>

</html>