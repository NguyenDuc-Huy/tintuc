<script src="https://code.highcharts.com/highcharts.js"></script>
<div class="card h-100">
    <div class="card-body">
        <h3 class="card-title text-center text-uppercase">Độc giả đăng kí mới</h3>

        <div class="container">
            <div id="container" style="width: 100%; height: 400px;"></div>
        </div>
        <script>
            $(function() {
                $.post('dataDG.php', {}, function(data) {
                    var data = jQuery.parseJSON(data);
                    console.log(data);
                    Highcharts.chart('container', {
                        chart: {
                            type: 'spline'
                        },
                        title: {
                            text: ''
                        },
                        xAxis: {
                            title: {
                                text: 'Tháng'
                            },
                            categories: data[0]
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Số độc giả'
                            }
                        },
                        tooltip: {
                            crosshairs: true,
                            shared: true
                        },
                        plotOptions: {
                            spline: {
                                marker: {
                                    radius: 4,
                                    lineColor: '#666666',
                                    lineWidth: 1
                                }
                            }
                        },
                        series: [{
                            name: 'Số độc giả',
                            data: data[1],

                        }]
                    });
                });

            });
        </script>

    </div>
</div>
<!-- footer -->