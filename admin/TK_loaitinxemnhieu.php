<script src="https://code.highcharts.com/highcharts.js"></script>
<div class="card h-100">
    <div class="card-body">
        <h3 class="card-title text-center text-uppercase">Top 10 loại tin được xem nhiều nhất</h3>

        <div class="container">
            <div id="container" style="width: 100%; height: 400px;"></div>
        </div>
        <script>
            $(function() {
                $.post('dataLoaiXN.php', {}, function(data) {
                    var data = jQuery.parseJSON(data);
                    console.log(data);
                    Highcharts.chart('container', {
                        title: {
                            text: ''
                        },
                        xAxis: {
                            categories: data[0]
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Số lượt xem'
                            }
                        },
                        series: [{
                            name: 'Số lượt xem',
                            data: data[1],
                            type: 'bar'
                        }]
                    });
                });

            });
        </script>

    </div>
</div>
<!-- footer -->