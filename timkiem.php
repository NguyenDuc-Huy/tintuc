<?php
include './includes/header.php';
?>
<?php
function doimau($str, $tukhoa)
{
    return str_replace($tukhoa, "<span style='color:red'>$tukhoa</span>", $str);
}
?>
<section id="contentSection">
    <div class="row">
        <div class="col-sm-8">
            <h3 style="margin:-3px 0 0 0">TÌM KIẾM VỚI TỪ KHÓA: <i><b><?php echo $_GET['tukhoa']; ?></b></i></h3>
        </div>
        <div class="col-sm-4 text-right" style="margin:-10px">
            <form class="form-inline" method="GET" action="">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="tukhoa" size="30" placeholder="Nhập từ khóa tìm kiếm">
                        <div class="input-group-addon"><button type="submit" style="border: none; "><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-12" style="margin-top: 10px;">
            <hr style="margin-top:0">
        </div>


        <div class="col-lg-8 col-md-8 col-sm-8">

            <div class="single_page ">
                <div class="single_page_content">
                    <div class="row">

                        <div class="col-sm-12">

                            <?php

                            $bantin = new bantin();
                            $rowsPerPage = 10;
                            if (!isset($_GET['page'])) {
                                $_GET['page'] = 1;
                            }
                            $offset = ($_GET['page'] - 1) * $rowsPerPage;

                            $tintk = $bantin->showTinTK($_GET['tukhoa'], $offset, $rowsPerPage);
                            if ($tintk) {

                                $numRows = $tintk->num_rows;

                                $maxPage = ceil($numRows / $rowsPerPage);

                                if ($numRows <> 0) {

                                    while ($e = $tintk->fetch_assoc()) {

                            ?>
                                        <div class="col-sm-12">

                                            <div class="row list_tin">
                                                <div class="col-sm-4 text-left" style="padding: 0;"> <a href="tin.php?tinId=<?= $e['tinId'] ?>" class="hinh_tin">
                                                        <?php
                                                        if ($e['linkvideo'] == NULL) echo ' <img alt="" src="admin/upload/images/' . $e['hinh'] . '">';
                                                        else echo '<span>' . $e['linkvideo'] . '</span>'
                                                        ?> </a>
                                                </div>
                                                <div class="col-sm-8">
                                                    <h4> <a href="tin.php?tinId=<?= $e['tinId'] ?>"> <?php if (isset($_GET['tukhoa'])) echo doimau($e['tieude'], $_GET['tukhoa']);
                                                                                                        else echo $e['tieude'] ?></a></h4>
                                                    <?php if (isset($_GET['tukhoa'])) echo doimau($e['tomtat'], $_GET['tukhoa']);
                                                    else echo $e['tomtat'] ?>
                                                </div>
                                            </div>
                                            <hr class="my-5">
                                        </div>

                            <?php

                                    }

                                    echo "<div style='text-align:center; font-size:16px; padding-top:20px'>";
                                    $re = $bantin->showTinTK_P($_GET['tukhoa']);
                                    $numRows = $re->num_rows;
                                    $maxPage = floor($numRows / $rowsPerPage) + 1;
                                    if ($_GET['page'] > 1) {
                                        echo " <button style='margin-left: 5px' class='btn'><a href=" . $_SERVER['PHP_SELF'] . "?tukhoa=" . $_GET['tukhoa'] . "&page=1"  . "><<&nbsp</a></button>";
                                        echo "<button style='margin-left: 5px' class='btn' ><a href=" . $_SERVER['PHP_SELF'] . "?tukhoa=" . $_GET['tukhoa'] . "&page=" . ($_GET['page'] - 1) . "><</a> </button>"; //gắn thêm nút Back
                                    }
                                    for ($i = 1; $i <= $maxPage; $i++) {
                                        if ($i == $_GET['page']) {
                                            echo '<button style="margin-left: 5px" class="btn btn-darkred"><b>' . $i . '</b> </button> '; //trang hiện tại sẽ được bôi đậm
                                        } else echo " <button style='margin-left: 5px' class='btn'><a href=" . $_SERVER['PHP_SELF'] . "?tukhoa=" . $_GET['tukhoa'] . "&page=" . $i . "> " . $i . "</a> </button> ";
                                    }
                                    if (
                                        $_GET['page'] < $maxPage
                                    ) {
                                        echo " <button style='margin-left: 5px' class='btn'><a href=" . $_SERVER['PHP_SELF'] . "?tukhoa=" . $_GET['tukhoa'] . "&page=" . ($_GET['page'] + 1) . ">></a> </button> ";
                                        echo " <button style='margin-left: 5px'class='btn'><a href=" . $_SERVER['PHP_SELF'] . "?tukhoa=" . $_GET['tukhoa'] . "&page=$maxPage"  . ">&nbsp>></a> </button>";  //gắn thêm nút Next
                                    }
                                    echo "</div>";
                                    //echo 'Tong so trang la: ' . $maxPage;
                                }
                            }

                            ?>
                        </div>


                        <div class="social_link">
                            <hr class="my-5">
                            <ul class="sociallink_nav">
                                <li>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-linkedin"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-pinterest"></i></a>
                                </li>
                            </ul>
                        </div>


                    </div>

                </div>
            </div>



        </div>



        <div class="col-lg-4 col-md-4 col-sm-4">
            <aside class="right_content">
                <div class="single_sidebar">
                    <h2><span>Tin xem nhiều</span></h2>
                    <ul class="spost_nav">
                        <?php
                        $tin_xn = $bantin->showTinPop();
                        if ($tin_xn) {
                            while ($tin_xn_e = $tin_xn->fetch_assoc()) {

                        ?>
                                <li>
                                    <div class="media wow fadeInDown">
                                        <a href="tin.php?tinId=<?= $tin_xn_e['tinId'] ?>" class="media-left">
                                            <img alt="" src="admin/upload/images/<?php echo $tin_xn_e['hinh'] ?>" />
                                        </a>
                                        <div class="media-body">
                                            <a href="tin.php?tinId=<?= $tin_xn_e['tinId'] ?>" class="catg_title">
                                                <?php echo '<b>' .  $tin_xn_e['tieude'] . '</b>';
                                                echo '<span style="font-size:12px">' . $bantin->textShorten($tin_xn_e['tomtat'], 100) . '</span>';
                                                ?>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                        <?php
                            }
                        } ?>

                    </ul>
                </div>
                <div class="single_sidebar">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#subcategory" aria-controls="home" role="tab" data-toggle="tab">Loại tin</a>
                        </li>
                        <li role="presentation">
                            <a href="#video" aria-controls="profile" role="tab" data-toggle="tab">Video</a>
                        </li>
                        <li role="presentation">
                            <a href="#comments" aria-controls="messages" role="tab" data-toggle="tab">Bình luận</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="subcategory">
                            <ul>
                                <?php
                                $tenlt = $loaitin->showLT();
                                if ($tenlt) {
                                    while ($tenlt_e = $tenlt->fetch_assoc()) {
                                        echo '<li class="cat-item"><a href="loaitinpage.php?loaitinId=' . $tenlt_e['loaitinId'] . '">' . $tenlt_e['tenloaitin'] . '</a></li>';
                                    }
                                }
                                ?>


                            </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="video">
                            <div class="vide_area">
                                <iframe width="100%" height="250" src="https://www.youtube.com/embed/AHRzZFXCnxk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="comments">
                            <ul class="spost_nav">
                                <li>
                                    <div class="media wow fadeInDown">
                                        <a href="single_page.html" class="media-left">
                                            <img alt="" src="public/images/huy.jpg" />
                                        </a>
                                        <div class="media-body">
                                            <a href="single_page.html" class="catg_title">
                                                Aliquam malesuada diam eget turpis varius 1</a>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="single_sidebar wow fadeInDown">
                    <h2><span>Sponsor</span></h2>
                    <a class="sideAdd" href="#"><img src="./public/images/sponsor.jpg" alt="" /></a>
                </div>

                <div class="single_sidebar wow fadeInDown">
                    <a class="sideAdd" href="#"><img src="public/images/trex.gif" alt=""></a>
                    <a class="sideAdd" href="#"><img src="public/images/tanminhchi.gif" alt=""></a>
                    <a class="sideAdd" href="#"><img src="public/images/tiger.gif" alt=""></a>

                </div>

            </aside>
        </div>
    </div>
</section>

<?php include './includes/footer.php' ?>