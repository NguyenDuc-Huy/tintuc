<?php
$filepath = realpath(dirname(__FILE__));
include_once($filepath . '/../lib/database.php');
?>
<?php
/**
 * 
 */
/**
 * 
 */

class bantin
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }
    public function insertTin($data, $files)
    {
        $tieude = mysqli_real_escape_string($this->db->link, $data['tieude']);
        $tomtat = mysqli_real_escape_string($this->db->link, $data['tomtat']);
        $noidung = mysqli_real_escape_string($this->db->link, $data['noidung']);
        $video = mysqli_real_escape_string($this->db->link, $data['video']);
        $trangthai = mysqli_real_escape_string($this->db->link, $data['trangthai']);
        $noibat = mysqli_real_escape_string($this->db->link, $data['noibat']);
        $theloaiId = mysqli_real_escape_string($this->db->link, $data['theloaiId']);
        $loaitinId = mysqli_real_escape_string($this->db->link, $data['loaitinId']);

        $permited = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_temp = $_FILES['image']['tmp_name'];

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
        $uploaded_image = "./upload/images/" . $unique_image;

        if (
            $tieude == "" || $tomtat == ""  || $trangthai == ""
            || $noibat == "" || $theloaiId == "" || $loaitinId == ""
        ) {
            $_SESSION['response'] = "Vui lòng nhập đầy đủ các thông tin";
            $_SESSION['res_type'] = "warning";
        } else if ($file_name == "") {
            $query = "INSERT INTO tintuc(tieude, tomtat, noidung, linkvideo, noibat, trangthai, loaitinId, theloaiId) 
            values ('$tieude','$tomtat', '$noidung','$video', '$noibat','$trangthai', '$loaitinId', '$theloaiId')";
        } else if (in_array($file_ext, $permited) === false) {
            $_SESSION['response'] = "Chỉ hỗ trợ upload file JPG, JPEG, PNG hoặc GIF";
            $_SESSION['res_type'] = "warning";
        } else if ($file_size > 1048567) {
            $_SESSION['response'] = "Vui lòng chọn ảnh có kích thước < 1MB";
            $_SESSION['res_type'] = "warning";
        } else {
            $query = "INSERT INTO tintuc(tieude, tomtat, noidung, linkvideo, hinh, noibat, trangthai, loaitinId, theloaiId) 
            values ('$tieude','$tomtat', '$noidung','$video', '$unique_image','$noibat','$trangthai', '$loaitinId', '$theloaiId')";
            move_uploaded_file($file_temp, $uploaded_image);
        }

        $result = $this->db->insert($query);
        if ($result) {
            $_SESSION['response'] = "Thêm mới thành công";
            $_SESSION['res_type'] = "success";
        } else {
            $_SESSION['response'] = "Thêm mới không thành công";
            $_SESSION['res_type'] = "danger";
        }
    }

    public function showTin()
    {
        $query = "SELECT * FROM tintuc ORDER BY tinId DESC";
        $result = $this->db->select($query);
        return $result;
    }
    public function showTintxt()
    {
        $query = "SELECT * FROM tintuc where (trangthai=1 and  linkvideo is NULL or linkvideo ='' and hinh is not NULL) ORDER BY tinId DESC";
        $result = $this->db->select($query);
        return $result;
    }
    public function showTinVideo()
    {
        $query = "SELECT * FROM tintuc where (trangthai=1 and linkvideo <>'') ORDER BY tinId DESC LIMIT 6";
        $result = $this->db->select($query);
        return $result;
    }
    public function showLastedNew()
    {
        $query = "SELECT * FROM tintuc  where trangthai=1 and linkvideo is null or linkvideo ='' and trangthai = 1 ORDER BY tinId DESC limit 10";
        $result = $this->db->select($query);
        return $result;
    }
    public function showFeaturePost()
    {
        $query = "SELECT * FROM tintuc where (noibat = 1 and trangthai=1) LIMIT 5 offset 9";
        $result = $this->db->select($query);
        return $result;
    }
    public function showTinXX($name, $limit, $offset)
    {
        $query = "SELECT tt.* FROM tintuc tt join theloai tl on tt.theloaiId=tl.theloaiId where (tt.noibat = 1 and tl.tentheloai like '%$name%' and tt.trangthai=1) LIMIT $limit offset $offset";
        $result = $this->db->select($query);
        return $result;
    }
    public function showTinPop()
    {
        $query = "SELECT * FROM tintuc where trangthai=1 order by soluotxem desc LIMIT 4";
        $result = $this->db->select($query);
        return $result;
    }
    public function showTinXemN($Id)
    {
        $query = "SELECT * FROM tintuc where trangthai=1 and theloaiId='$Id' order by soluotxem desc LIMIT 4";
        $result = $this->db->select($query);
        return $result;
    }
    public function tinxemnhieu_loai($id)
    {
        $query = "SELECT * FROM tintuc where trangthai=1 and loaitinId='$id' order by soluotxem desc LIMIT 5";
        $result = $this->db->select($query);
        return $result;
    }
    public function showTinPopTLLT($theloaiId, $limit)
    {
        $query = "SELECT * FROM tintuc tt where trangthai=1 and noibat = 1 and theloaiId='$theloaiId' and linkvideo is null or linkvideo ='' LIMIT $limit";
        $result = $this->db->select($query);
        return $result;
    }
    public function showTinTLLT($id, $loaitin, $limit, $offset)
    {
        $query = "SELECT * FROM tintuc tt join loaitin lt on tt.loaitinId=lt.loaitinId where tt.trangthai=1 and tt.theloaiId ='$id' and lt.tenloaitin like '%$loaitin%' limit $limit offset $offset";
        $result = $this->db->select($query);
        return $result;
    }
    public function showTinLT($loaitinId, $limit, $offset)
    {
        $query = "SELECT * FROM tintuc where trangthai=1 and loaitinId = '$loaitinId' limit $limit, $offset";
        $result = $this->db->select($query);
        return $result;
    }

    public function showTinTimKiem($loaitinId, $tukhoa, $limit, $offset)
    {
        $query = "SELECT * FROM tintuc where (trangthai=1 and loaitinId = '$loaitinId') and (tieude like '%$tukhoa%' OR tomtat like '%$tukhoa%') limit $limit, $offset";
        $result = $this->db->select($query);
        return $result;
    }
    public function showTinTK($tukhoa, $limit, $offset)
    {
        $query = "SELECT * FROM tintuc where trangthai=1 and (tieude like '%$tukhoa%' OR tomtat like '%$tukhoa%') limit $limit, $offset";
        $result = $this->db->select($query);
        return $result;
    }
    public function showTinTK_P($tukhoa)
    {
        $query = "SELECT * FROM tintuc where trangthai=1 and (tieude like '%$tukhoa%' OR tomtat like '%$tukhoa%')";
        $result = $this->db->select($query);
        return $result;
    }
    public function showTinLQ($loaitinId, $limit, $offset)
    {
        $query = "SELECT * FROM tintuc where trangthai=1 and loaitinId = '$loaitinId' limit $limit offset $offset";
        $result = $this->db->select($query);
        return $result;
    }
    public function showTinPaging($loaitinId)
    {
        $query = "SELECT * FROM tintuc tt where trangthai=1 and loaitinId = '$loaitinId'";
        $result = $this->db->select($query);
        return $result;
    }
    public function showTinTKPaging($loaitinId, $tukhoa)
    {
        $query = "SELECT * FROM tintuc tt where (trangthai=1 and loaitinId = '$loaitinId') and (tieude like '%$tukhoa%' OR tomtat like '%$tukhoa%')";
        $result = $this->db->select($query);
        return $result;
    }
    public function getTin($id)
    {
        $query = "SELECT * FROM tintuc WHERE tinId=$id";
        $result = $this->db->select($query);
        return $result;
    }
    public function updateLuotXem($id)
    {
        $query = "UPDATE tintuc SET soluotxem = soluotxem + 1 WHERE tinId = '$id'";
        $result = $this->db->update($query);
    }

    public function updateTin($data, $files, $id)
    {

        $tieude = mysqli_real_escape_string($this->db->link, $data['tieude']);
        $tomtat = mysqli_real_escape_string($this->db->link, $data['tomtat']);
        $noidung = mysqli_real_escape_string($this->db->link, $data['noidung']);
        $video = mysqli_real_escape_string($this->db->link, $data['video']);
        $trangthai = mysqli_real_escape_string($this->db->link, $data['trangthai']);
        $noibat = mysqli_real_escape_string($this->db->link, $data['noibat']);
        $theloaiId = mysqli_real_escape_string($this->db->link, $data['theloaiId']);
        $loaitinId = mysqli_real_escape_string($this->db->link, $data['loaitinId']);
        $hinh = mysqli_real_escape_string($this->db->link, $data['hinh']);

        $permited = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_temp = $_FILES['image']['tmp_name'];
        var_dump($file_name);

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
        $uploaded_image = "./upload/images/" . $unique_image;

        if (
            $tieude == "" || $tomtat == "" || $trangthai == ""
            || $noibat == "" || $theloaiId == "" || $loaitinId == ""
        ) {
            $_SESSION['response'] = "Vui lòng nhập đầy đủ các thông tin";
            $_SESSION['res_type'] = "warning";
        } else {
            if ($file_name != null) {
                if (in_array($file_ext, $permited) === false) {
                    $_SESSION['response'] = "Chỉ hỗ trợ upload file JPG, JPEG, PNG hoặc GIF";
                    $_SESSION['res_type'] = "warning";
                } else if ($file_size > 1048567) {
                    $_SESSION['response'] = "Vui lòng chọn ảnh có kích thước < 1MB";
                    $_SESSION['res_type'] = "warning";
                }
                unlink('./upload/images/' . $hinh);
                move_uploaded_file($file_temp, $uploaded_image);
                $query = "UPDATE tintuc SET tieude = '$tieude', tomtat='$tomtat', noidung='$noidung', linkvideo='$video', hinh='$unique_image', noibat='$noibat',
                 trangthai='$trangthai', loaitinId='$loaitinId', theloaiId='$theloaiId' where tinId= '$id'";
            } else {
                $query = "UPDATE tintuc SET tieude = '$tieude', tomtat='$tomtat', noidung='$noidung', linkvideo='$video', noibat='$noibat',
                 trangthai='$trangthai', loaitinId='$loaitinId', theloaiId='$theloaiId' where tinId= '$id'";
            }

            $result = $this->db->update($query);
            if ($result) {
                $_SESSION['response'] = "Cập nhật thành công";
                $_SESSION['res_type'] = "success";
            } else {
                $_SESSION['response'] = "Cập nhật không thành công";
                $_SESSION['res_type'] = "danger";
            }
        }
    }
    public function deleteTin($id)
    {
        $query = "DELETE FROM tintuc where tinId = '$id'";
        $result = $this->db->delete($query);

        if ($result) {
            $tindel = $this->getTin($id);
            if ($tindel) {
                while ($hinh = $tindel->fetch_assoc()) {
                    unlink('.upload/images/' . $hinh['hinh']);
                }
            }


            $_SESSION['response'] = "Xóa thành công";
            $_SESSION['res_type'] = "success";
        } else {
            $_SESSION['response'] = "Xóa không thành công";
            $_SESSION['res_type'] = "danger";
        }
    }
    public function SLtinTL($id)
    {
        $query = "SELECT count(*) as SL FROM tintuc where theloaiId ='$id'";
        $result = $this->db->select($query);
        return $result;
    }

    public function textShorten($text, $limit = 400)
    {
        $text = $text . " ";
        $text = substr($text, 0, $limit);
        $text = substr($text, 0, strrpos($text, ' '));
        $text = $text . "...";
        return $text;
    }
}
?>