<?php

$filepath = realpath(dirname(__FILE__));
include_once($filepath . '/../lib/database.php');
include_once($filepath . '/../lib/sendMail_class.php');

?>
<?php
/**
 * 
 */
/**
 * 
 */

class nhanvien
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }
    public function insertNV($data, $files)
    {
        $hoten = mysqli_real_escape_string($this->db->link, $data['hoten']);
        $gioitinh = mysqli_real_escape_string($this->db->link, $data['gioitinh']);
        $diachi = mysqli_real_escape_string($this->db->link, $data['diachi']);
        $sodt = mysqli_real_escape_string($this->db->link, $data['sodt']);
        $ngaybatdau = mysqli_real_escape_string($this->db->link, $data['ngaybatdau']);
        $tendangnhap = mysqli_real_escape_string($this->db->link, $data['username']);
        $matkhau = mysqli_real_escape_string($this->db->link, $data['password']);
        $email = mysqli_real_escape_string($this->db->link, $data['email']);
        $chucvu = mysqli_real_escape_string($this->db->link, $data['chucvu']);

        $mk = md5($matkhau);

        $permited = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_temp = $_FILES['image']['tmp_name'];

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
        $uploaded_image = "./upload/avatar/" . $unique_image;

        if (
            $hoten == "" || $gioitinh == "" || $diachi == "" || $sodt == ""
            || $ngaybatdau == "" || $tendangnhap == "" || $mk == "" || $email == "" || $chucvu == ""
        ) {
            $_SESSION['response'] = "Vui lòng nhập đầy đủ các thông tin";
            $_SESSION['res_type'] = "warning";
        } else if ($file_name == "") {
            $query = "INSERT INTO nhanvien(hotenNV, gioitinh, diachi, sodt, ngaybatdau, username, password, email, chucvu) 
            values ('$hoten','$gioitinh', '$diachi', '$sodt','$ngaybatdau','$tendangnhap', '$mk', '$email', '$chucvu')";
        } else if (in_array($file_ext, $permited) === false) {
            $_SESSION['response'] = "Chỉ hỗ trợ upload file JPG, JPEG, PNG hoặc GIF";
            $_SESSION['res_type'] = "warning";
        } else if ($file_size > 1048567) {
            $_SESSION['response'] = "Vui lòng chọn ảnh có kích thước < 1MB";
            $_SESSION['res_type'] = "warning";
        } else {
            $query = "INSERT INTO nhanvien(hotenNV, gioitinh, diachi, sodt,anhdaidien, ngaybatdau, username, password, email, chucvu) 
            values ('$hoten','$gioitinh', '$diachi', '$sodt','$unique_image','$ngaybatdau','$tendangnhap', '$mk', '$email', '$chucvu')";
            move_uploaded_file($file_temp, $uploaded_image);
        }
        $result = $this->db->insert($query);
        if ($result) {
            $send = new Mailer();
            $send->sendMailCF($email, $hoten, $tendangnhap, $matkhau);
            $_SESSION['response'] = "Thêm mới thành công";
            $_SESSION['res_type'] = "success";
        } else {
            $_SESSION['response'] = "Thêm mới không thành công";
            $_SESSION['res_type'] = "danger";
        }
    }

    public function showNV()
    {
        $query = "SELECT * FROM nhanvien ORDER BY Id DESC";
        $result = $this->db->select($query);
        return $result;
    }

    public function getNV($id)
    {
        $query = "SELECT * FROM nhanvien WHERE Id='$id'";
        $result = $this->db->select($query);
        return $result;
    }

    public function updateNV($data, $files, $id)
    {
        $hoten = mysqli_real_escape_string($this->db->link, $data['hoten']);
        $gioitinh = mysqli_real_escape_string($this->db->link, $data['gioitinh']);
        $diachi = mysqli_real_escape_string($this->db->link, $data['diachi']);
        $sodt = mysqli_real_escape_string($this->db->link, $data['sodt']);
        $ngaybatdau = mysqli_real_escape_string($this->db->link, $data['ngaybatdau']);
        $email = mysqli_real_escape_string($this->db->link, $data['email']);
        $chucvu = mysqli_real_escape_string($this->db->link, $data['chucvu']);
        $hinh = mysqli_real_escape_string($this->db->link, $data['hinh']);

        $permited = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_temp = $_FILES['image']['tmp_name'];

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
        $uploaded_image = "./upload/avatar/" . $unique_image;


        if (
            $hoten == "" || $gioitinh == "" || $diachi == "" || $sodt == ""
            || $ngaybatdau == "" || $email == "" || $chucvu == ""
        ) {
            $_SESSION['response'] = "Vui lòng nhập đầy đủ các thông tin";
            $_SESSION['res_type'] = "warning";
        } else {
            if ($file_name != null) {
                if (in_array($file_ext, $permited) === false) {
                    $_SESSION['response'] = "Chỉ hỗ trợ upload file JPG, JPEG, PNG hoặc GIF";
                    $_SESSION['res_type'] = "warning";
                } else if ($file_size > 1048567) {
                    $_SESSION['response'] = "Vui lòng chọn ảnh có kích thước < 1MB";
                    $_SESSION['res_type'] = "warning";
                }
                unlink('./upload/avatar/' . $hinh);
                move_uploaded_file($file_temp, $uploaded_image);
                $query = "UPDATE nhanvien SET hotenNV = '$hoten', gioitinh='$gioitinh', diachi='$diachi', anhdaidien='$unique_image', sodt='$sodt',
                 ngaybatdau='$ngaybatdau', email='$email', chucvu='$chucvu' where Id= '$id'";
            } else {
                $query = "UPDATE nhanvien SET hotenNV = '$hoten', gioitinh='$gioitinh', diachi='$diachi', sodt='$sodt',
                 ngaybatdau='$ngaybatdau', email='$email', chucvu='$chucvu' where Id= '$id'";
            }

            $result = $this->db->update($query);
            if ($result) {
                $_SESSION['response'] = "Cập nhật thành công";
                $_SESSION['res_type'] = "success";
            } else {
                $_SESSION['response'] = "Cập nhật không thành công";
                $_SESSION['res_type'] = "danger";
            }
        }
    }
    public function deleteNV($id)
    {
        $query = "DELETE FROM nhanvien where Id = '$id'";
        $result = $this->db->delete($query);

        if ($result) {
            $tindel = $this->getNV($id);
            if ($tindel) {
                $hinh = $tindel->fetch_assoc();
                unlink('./upload/avatar/' . $hinh['anhdaidien']);
            }

            $_SESSION['response'] = "Xóa thành công";
            $_SESSION['res_type'] = "success";
        } else {
            $_SESSION['response'] = "Xóa không thành công";
            $_SESSION['res_type'] = "danger";
        }
    }
}
?>