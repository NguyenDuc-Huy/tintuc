<?php
$filepath = realpath(dirname(__FILE__));
include_once($filepath . '/../lib/database.php');
?>
<?php
/**
 * 
 */
/**
 * 
 */

class loaitin
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }
    public function insertLT($data)
    {

        $tenloaitin = mysqli_real_escape_string($this->db->link, $data['tenloaitin']);
        $trangthai = mysqli_real_escape_string($this->db->link, $data['trangthai']);
        $theloaiId = mysqli_real_escape_string($this->db->link, $data['theloaiId']);
        if ($tenloaitin == "" || $trangthai == "" || $theloaiId == "") {
            $_SESSION['response'] = "Vui lòng nhập đầy đủ các thông tin";
            $_SESSION['res_type'] = "warning";
        } else {
            $query = "INSERT INTO loaitin (tenloaitin, trangthai, theloaiId) values ('$tenloaitin','$trangthai','$theloaiId')";

            $result = $this->db->insert($query);
            if ($result) {
                $_SESSION['response'] = "Thêm mới thành công";
                $_SESSION['res_type'] = "success";
            } else {
                $_SESSION['response'] = "Thêm mới không thành công";
                $_SESSION['res_type'] = "danger";
            }
        }
    }

    public function showLT()
    {
        $query = "SELECT * FROM loaitin ORDER BY loaitinId DESC";
        $result = $this->db->select($query);
        return $result;
    }

    public function getLT($id)
    {
        $query = "SELECT * FROM loaitin WHERE loaitinId='$id'";
        $result = $this->db->select($query);
        return $result;
    }
    public function getLTTL($id)
    {
        $query = "SELECT * FROM loaitin WHERE theloaiId='$id'";
        $result = $this->db->select($query);
        return $result;
    }

    public function updateLT($data, $id)
    {

        $tenloaitin = mysqli_real_escape_string($this->db->link, $data['tenloaitin']);
        $trangthai = mysqli_real_escape_string($this->db->link, $data['trangthai']);


        if ($tenloaitin == "" or $trangthai == "") {
            $_SESSION['response'] = "Vui lòng nhập đầy đủ các thông tin";
            $_SESSION['res_type'] = "warning";
        } else {
            $query = "UPDATE loaitin set tenloaitin = '$tenloaitin', trangthai='$trangthai' where loaitinId = '$id'";
            $result = $this->db->update($query);
            if ($result) {
                $_SESSION['response'] = "Cập nhật thành công";
                $_SESSION['res_type'] = "success";
            } else {
                $_SESSION['response'] = "Cập nhật không thành công";
                $_SESSION['res_type'] = "danger";
            }
        }
    }
    public function deleteLT($id)
    {
        $query = "DELETE FROM loaitin where loaitinId = '$id'";
        $result = $this->db->delete($query);

        if ($result) {
            $_SESSION['response'] = "Xóa thành công";
            $_SESSION['res_type'] = "success";
        } else {
            $_SESSION['response'] = "Xóa không thành công";
            $_SESSION['res_type'] = "danger";
        }
    }
    public function LTXemNhieu()
    {
        $query = "SELECT lt.tenloaitin, sum(tt.soluotxem) as SLX 
        FROM tintuc tt join loaitin lt on lt.loaitinId = tt.loaitinId 
        GROUP BY lt.tenloaitin
        ORDER BY sum(tt.soluotxem) DESC
        LIMIT 10";
        $result = $this->db->select($query);
        return $result;
    }
}
?>