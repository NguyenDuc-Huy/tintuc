<?php
$filepath = realpath(dirname(__FILE__));
include_once($filepath . '/../lib/database.php');
?>
<?php
/**
 * 
 */
/**
 * 
 */

class theloai
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }
    public function insertTL($data)
    {

        $tentheloai = mysqli_real_escape_string($this->db->link, $data['tentheloai']);
        $anhien = mysqli_real_escape_string($this->db->link, $data['anhien']);
        if ($tentheloai == "" || $anhien == "") {
            $_SESSION['response'] = "Vui lòng nhập đầy đủ các thông tin";
            $_SESSION['res_type'] = "warning";
        } else {
            $query = "INSERT INTO theloai(tentheloai, anhien) values ('$tentheloai','$anhien')";

            $result = $this->db->insert($query);
            if ($result) {
                $_SESSION['response'] = "Thêm mới thành công";
                $_SESSION['res_type'] = "success";
            } else {
                $_SESSION['response'] = "Thêm mới không thành công";
                $_SESSION['res_type'] = "danger";
            }
        }
    }

    public function showTL()
    {
        $query = "SELECT * FROM theloai ORDER BY theloaiId DESC";
        $result = $this->db->select($query);
        return $result;
    }

    public function getTL($id)
    {
        $query = "SELECT * FROM theloai WHERE theloaiId='$id'";
        $result = $this->db->select($query);
        return $result;
    }

    public function updateTL($data, $id)
    {

        $tentheloai = mysqli_real_escape_string($this->db->link, $data['tentheloai']);
        $anhien = mysqli_real_escape_string($this->db->link, $data['anhien']);

        if ($tentheloai == "" or $anhien == "") {
            $_SESSION['response'] = "Vui lòng nhập đầy đủ các thông tin";
            $_SESSION['res_type'] = "warning";
        } else {
            $query = "UPDATE theloai set tentheloai = '$tentheloai', anhien='$anhien' where theloaiId = '$id'";
            $result = $this->db->update($query);
            if ($result) {
                $_SESSION['response'] = "Cập nhật thành công";
                $_SESSION['res_type'] = "success";
            } else {
                $_SESSION['response'] = "Cập nhật không thành công";
                $_SESSION['res_type'] = "danger";
            }
        }
    }
    public function deleteTL($id)
    {
        $query = "DELETE FROM theloai where theloaiId = '$id'";
        $result = $this->db->delete($query);

        if ($result) {
            $_SESSION['response'] = "Xóa thành công";
            $_SESSION['res_type'] = "success";
        } else {
            $_SESSION['response'] = "Xóa không thành công";
            $_SESSION['res_type'] = "danger";
        }
    }
}
?>