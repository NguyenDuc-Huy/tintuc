<?php

$filepath = realpath(dirname(__FILE__));
include_once($filepath . '/../lib/database.php');
include_once($filepath . '/../lib/sendMail_class.php');
?>
<?php
/**
 * 
 */
/**
 * 
 */

class docgia
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }
    public function insertDG($data)
    {
        $username = mysqli_real_escape_string($this->db->link, $data['name']);
        $email = mysqli_real_escape_string($this->db->link, $data['email']);
        $mk = mysqli_real_escape_string($this->db->link, $data['password']);
        $re_mk = mysqli_real_escape_string($this->db->link, $data['re_password']);




        if ($username == "" || $mk == "" || $email == "" || $re_mk == "") {
            $alert = "Vui lòng điền đầy đủ các thông tin";
            return $alert;
        } else {
            $query = "SELECT * from user where name='$username'";
            $result = $this->db->select($query);
            if ($result) {
                $alert = "<i style='color:red'>Tên đăng nhập đã tồn tại</i>";
                return $alert;
            } else {
                $query = "SELECT * from nhanvien where username='$username'";
                $result = $this->db->select($query);
                if ($result) {
                    $alert = "<i style='color:red'>Tên đăng nhập đã tồn tại</i>";
                    return $alert;
                }
            }
        }
        if ($mk !== $re_mk) {
            $alert = "Nhập lại mật khẩu không đúng!";
            return $alert;
        } else {
            $mk = md5($mk);
            $query = "INSERT INTO user(name, email, password) values ('$username','$email', '$mk')";
            $result = $this->db->insert($query);
            if ($result) {
                $send = new Mailer();
                $send->sendMailCF($email, 'Độc giả Tintuc 24h', $username, $re_mk);
                header('Location:login.php');
            }
        }
    }

    public function showDG()
    {
        $query = "SELECT * FROM user ORDER BY userId DESC";
        $result = $this->db->select($query);
        return $result;
    }

    public function getDG($id)
    {
        $query = "SELECT * FROM user WHERE userId='$id'";
        $result = $this->db->select($query);
        return $result;
    }
    public function updateStatus($id, $value)
    {
        $Id = mysqli_real_escape_string($this->db->link, $id);
        $value = mysqli_real_escape_string($this->db->link, $value);

        $query = "UPDATE user set active='$value' where userId = '$Id'";

        $result = $this->db->update($query);
        if ($result) {
            $_SESSION['response'] = "Cập nhật trạng thái thành công";
            $_SESSION['res_type'] = "success";
        } else {
            $_SESSION['response'] = "Cập nhật trạng thái không thành công";
            $_SESSION['res_type'] = "danger";
        }
    }
    public function updateDG($data, $files, $id)
    {
        $hoten = mysqli_real_escape_string($this->db->link, $data['hoten']);
        $gioitinh = mysqli_real_escape_string($this->db->link, $data['gioitinh']);
        $email = mysqli_real_escape_string($this->db->link, $data['email']);
        $active = mysqli_real_escape_string($this->db->link, $data['active']);
        $hinh = mysqli_real_escape_string($this->db->link, $data['hinh']);

        $permited = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_temp = $_FILES['image']['tmp_name'];

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
        $uploaded_image = "./upload/avatar/" . $unique_image;


        if (
            $hoten == "" || $gioitinh == "" || $email == "" || $active == ""
        ) {
            $_SESSION['response'] = "Vui lòng nhập đầy đủ các thông tin";
            $_SESSION['res_type'] = "warning";
        } else {
            if ($file_name != null) {
                if (in_array($file_ext, $permited) === false) {
                    $_SESSION['response'] = "Chỉ hỗ trợ upload file JPG, JPEG, PNG hoặc GIF";
                    $_SESSION['res_type'] = "warning";
                } else if ($file_size > 1048567) {
                    $_SESSION['response'] = "Vui lòng chọn ảnh có kích thước < 1MB";
                    $_SESSION['res_type'] = "warning";
                }
                unlink('./upload/avatar/' . $hinh);
                move_uploaded_file($file_temp, $uploaded_image);
                $query = "UPDATE user SET name = '$hoten', gioitinh='$gioitinh', anhdaidien='$unique_image', email='$email',
                active='$active' where userId= '$id'";
            } else {
                $query = "UPDATE user SET name = '$hoten', gioitinh='$gioitinh', email='$email', active='$active' where userId= '$id'";
            }

            $result = $this->db->update($query);
            if ($result) {
                $_SESSION['response'] = "Cập nhật thành công";
                $_SESSION['res_type'] = "success";
            } else {
                $_SESSION['response'] = "Cập nhật không thành công";
                $_SESSION['res_type'] = "danger";
            }
        }
    }
    public function deleteDG($id)
    {
        $query = "DELETE FROM user where userId = '$id'";
        $result = $this->db->delete($query);

        if ($result) {
            $tindel = $this->getDG($id);
            if ($tindel) {
                while ($hinh = $tindel->fetch_assoc())
                    unlink('./upload/avatar/' . $hinh['anhdaidien']);
            }


            $_SESSION['response'] = "Xóa thành công";
            $_SESSION['res_type'] = "success";
        } else {
            $_SESSION['response'] = "Xóa không thành công";
            $_SESSION['res_type'] = "danger";
        }
    }


    public function soDGThang($thang)
    {
        $query = "SELECT count(*) as SDG from user where MONTH(ngaydk) = '$thang'";
        $result = $this->db->select($query);
        return $result;
    }
}
?>