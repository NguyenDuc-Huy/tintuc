<?php
$filepath = realpath(dirname(__FILE__));
include_once($filepath . '/../lib/database.php');
?>
<?php
/**
 * 
 */
/**
 * 
 */

class binhluan
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }
    public function themBL($userId, $tinId, $noidung)
    {

        $userId = mysqli_real_escape_string($this->db->link, $userId);
        $tinId = mysqli_real_escape_string($this->db->link, $tinId);
        $noidung = mysqli_real_escape_string($this->db->link, $noidung);
        if ($userId  == "" || $tinId == "" || $noidung == "") {
            $alert = "<i style='color:red'>Vui lòng nhập nội dung trước khi gửi</i>";
            return $alert;
        } else {
            $query = "INSERT INTO binhluan (userId, tinId, noidung) values ('$userId','$tinId','$noidung')";

            $result = $this->db->insert($query);
        }
    }

    public function showBL()
    {
        $query = "SELECT * FROM binhluan ORDER BY binhluanId DESC";
        $result = $this->db->select($query);
        return $result;
    }
    public function getBL($id)
    {
        $query = "SELECT * FROM binhluan where binhluanId='$id'";
        $result = $this->db->select($query);
        return $result;
    }
    public function showBLTheoTin($id)
    {
        $query = "SELECT * FROM binhluan where tinId = '$id' and trangthai = 1 ORDER BY binhluanId DESC";
        $result = $this->db->select($query);
        return $result;
    }



    public function updateBL($id, $value)
    {

        $tinId = mysqli_real_escape_string($this->db->link, $id);
        $value = mysqli_real_escape_string($this->db->link, $value);

        $query = "UPDATE binhluan set trangthai='$value' where binhluanId = '$tinId'";

        $result = $this->db->update($query);
        if ($result) {
            $_SESSION['response'] = "Cập nhật thành công";
            $_SESSION['res_type'] = "success";
        } else {
            $_SESSION['response'] = "Cập nhật không thành công";
            $_SESSION['res_type'] = "danger";
        }
    }
    public function deleteBL($id)
    {
        $query = "DELETE FROM binhluan where binhluanId = '$id'";
        $result = $this->db->delete($query);

        if ($result) {
            $_SESSION['response'] = "Xóa thành công";
            $_SESSION['res_type'] = "success";
        } else {
            $_SESSION['response'] = "Xóa không thành công";
            $_SESSION['res_type'] = "danger";
        }
    }
}
?>