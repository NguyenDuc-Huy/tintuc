<?php
$filepath = realpath(dirname(__FILE__));
include_once($filepath . '/../lib/database.php');
include_once($filepath . '/../lib/sendMail_class.php');
session_start();
?>

<?php

class check_login
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }
    public function validation($data)
    {
        $data = trim($data);
        $data = stripcslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    public function login_admin($data)
    {
        $username = $data['username'];
        $password = $data['password'];
        $name = $this->validation($username);
        $pass = $this->validation($password);
        $name = mysqli_real_escape_string($this->db->link, $name);
        $pass = mysqli_real_escape_string($this->db->link, $pass);

        if (empty($name) || empty($pass)) {
            $alert = "User and password must not be empty!";
            return $alert;
        } else {
            $query = "SELECT * FROM nhanvien where username = '$name'";
            $result = $this->db->select($query);
            if ($result == false) {
                $query2 = "SELECT * FROM user where name = '$name'";
                $result2 = $this->db->select($query2);
                if ($result2 == false) {
                    $alert = "This username does not exist. Please try again!";
                    return $alert;
                } else {
                    $value2 = $result2->fetch_assoc();
                    // mã hóa pasword
                    $pass = md5($pass);
                    if ($value2['active'] == 0) {
                        $alert = "Your account is inactive. Sorry!";
                        return $alert;
                    } else 
                    if ($pass != $value2['password']) {
                        $alert = "Password is incorrect. Please try again!";
                        return $alert;
                    } else {
                        $_SESSION['username'] = $name;
                        $_SESSION['userId'] = $value2['userId'];
                        header('Location:index.php');
                    }
                }
            } else {
                $value = $result->fetch_assoc();
                // mã hóa pasword
                $pass = md5($pass);
                if ($pass != $value['password']) {
                    $alert = "Password is incorrect. Please try again!";
                    return $alert;
                } else {
                    $_SESSION['username'] = $name;
                    $_SESSION['userId'] = $value['Id'];
                    header('Location:admin/index.php');
                }
            }
        }
    }
    public function forgetPws($data)
    {
        $username = $data['username'];
        $email = $data['email'];
        $name = mysqli_real_escape_string($this->db->link, $username);
        $email = mysqli_real_escape_string($this->db->link, $email);
        if (empty($username) || empty($email)) {
            $alert = "Username and email must not be empty!";
            return $alert;
        } else {
            $query = "SELECT * FROM nhanvien where username = '$name'";
            $result = $this->db->select($query);
            if ($result == false) {
                $query2 = "SELECT * FROM user where name = '$name'";
                $result2 = $this->db->select($query2);
                if ($result2 == false) {
                    $alert = "This username does not exist. Please try again!";
                    return $alert;
                } else {
                    $value2 = $result2->fetch_assoc();

                    if ($email != $value2['email']) {
                        $alert = "Email does not match. Please try again!";
                        return $alert;
                    } else {
                        $new_pw = substr(md5(time()), 0, 6);
                        $ens_pw = md5($new_pw);
                        $que2 = "UPDATE user set password = '$ens_pw' where name = '$username'";
                        $result = $this->db->update($que2);
                        if ($result) {
                            $send = new Mailer();
                            $send->sendMailCF($email, 'Độc giả Tintuc 24h', $name, $new_pw);
                            header('Location:login.php');
                        }
                    }
                }
            } else {
                $value = $result->fetch_assoc();
                if ($email != $value['email']) {
                    $alert = "Email does not match. Please try again!";
                    return $alert;
                } else {
                    $new_pw = substr(md5(time()), 0, 6);
                    $ens_pw = md5($new_pw);
                    $que1 = "UPDATE nhanvien set password = '$ens_pw' where username = '$username'";
                    $result = $this->db->update($que1);
                    if ($result) {
                        $send = new Mailer();
                        $send->sendMailCF($email, 'Độc giả Tintuc 24h', $name, $new_pw);
                        header('Location:login.php');
                    }
                }
            }
        }
    }
}
?>
