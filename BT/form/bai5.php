<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kết quả thi đại học</title>
</head>

<body>
    <?php
    if (isset($_POST['submit'])) {
        $gbd = $_POST['gbd'];
        $gkt = $_POST['gkt'];

        if (!is_numeric($gbd) or !is_numeric($gkt))
            $error = "Giờ nhập vào phải dạng số";
        else if ($gbd < 10 or $gbd > 24 or $gkt < 10 or $gkt > 24)
            $error = "Khung giờ phải từ 10h-24h";
        else if ($gkt <= $gbd)
            $error = "Giờ kết thúc phải lớn hơn giờ bắt đầu";
        else {
            if ($gkt <= 17)
                $tien = ($gkt - $gbd) * 20000;
            else if ($gbd > 17)
                $tien = ($gkt - $gbd) * 45000;
            else $tien = (17 - $gbd) * 20000 + ($gkt - 17) * 45000;
        }
    }
    ?>

    <form action="" method="POST">
        <table align="center" style="background-color: lightseagreen; padding: 2px;">
            <tr>
                <td colspan="2" style="background-color: seagreen; " align="center">
                    <h2 style="color: white;">TÍNH TIỀN KARAOKE</h2>
                </td>
            </tr>

            <tr>
                <td>Giờ bắt đầu: </td>
                <td><input type="text" name="gbd" required value="<?php if (isset($_POST['gbd'])) echo $_POST['gbd']; ?>">
                    <label for="">(h)</label>
                </td>

            </tr>
            <tr>
                <td>Giờ kết thúc: </td>
                <td><input type="text" name="gkt" required value="<?php if (isset($_POST['gkt'])) echo $_POST['gkt']; ?>">
                    <label for="">(h)</label>
                </td>
            </tr>
            <tr>
                <td>Tiền thanh toán: </td>
                <td><input type="text" readonly value="<?php if (isset($tien)) echo $tien; ?>"><label for=""> (VND)</label></td>
            </tr>

            <tr>
                <td colspan="2"> <?php if (isset($error)) echo "<br><i style='color:red';>" . $error . "</i>"; ?></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align: center;"><input type="submit" value="Tính tiền" name="submit"></td>
            </tr>
        </table>
    </form>
</body>

</html>