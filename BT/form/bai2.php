<?php

if (isset($_POST['submit'])) {
    $bk = $_POST['bankinh'];
    if (!is_numeric($bk)) $error = "Hãy nhập vào dạng số";
    else if ($bk <= 0) $error = "Bán kinh phải lớn hơn 0";
    else {
        $dientich = round($bk ** 2 * pi(), 3);
        $chuvi = round($bk * 2 * pi(), 3);
    }
}
?>

<form action="" method="POST">
    <table align="center" style="background-color: khaki; padding: 20px;">
        <tr>
            <td colspan="2">
                <h2 style="background-color: orange; text-align: center;">TÍNH HÌNH TRÒN</h2>
            </td>
        </tr>
        <tr>
            <td>Bán kính: </td>
            <td><input type="text" name="bankinh" required value="<?php if (isset($_POST['bankinh'])) echo $_POST['bankinh']; ?>"></td>

        </tr>

        <tr>
            <td>Chu vi: </td>
            <td><input type="text" value="<?php if (isset($chuvi)) echo $chuvi; ?>" readonly></td>
        </tr>
        <tr>
            <td>Diện tích: </td>
            <td><input type="text" value="<?php if (isset($dientich)) echo $dientich; ?>" readonly></td>

        </tr>
        <tr>
            <td colspan="2" style="vertical-align: middle;">
                <?php if (isset($error)) echo "<br><i style='color:red';>" . $error . "</i>"; ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align: center;"><input type="submit" value="Tính" name="submit"></td>
        </tr>
    </table>
</form>