<?php
function tinhtoan($so1, $so2, $pt)
{
    switch ($pt) {
        case 'Cộng':
            return $so1 + $so2;
            break;
        case 'Trừ':
            return  $so1 - $so2;
            break;
        case 'Nhân':
            return $so1 * $so2;
            break;
        default:
            if ($so2 == 0) return 'Lỗi chia cho 0';
            else return $so1 / $so2;
            break;
    }
}
$so1 = $_POST['so1'];
$so2 = $_POST['so2'];
if (!is_numeric($so1) or !is_numeric($so2)) {
    $kq = "Nhập lại hai số";
} else $kq = tinhtoan($so1, $so2, $_POST['pheptinh']);



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<style>
    section {
        position: absolute;
        left: 50%;
        top: 20%;
        transform: translate(-50%, -50%);
    }
</style>

<body style=" font-weight: bold;">
    <section>
        <h2>PHÉP TÍNH TRÊN HAI SỐ</h2>
        <form action="bai6_ketqua.php" method="POST">
            <table>
                <tr>
                    <td style="color: rgb(168, 15, 15)">Chọn phép tính</td>
                    <td>
                        <?php
                        if (isset($_POST['pheptinh'])) echo "<span style='color:red'>" . $_POST['pheptinh'] . "</span>";
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="color: blue">Số thứ nhất:</td>
                    <td><input type="text" name="so1" size="27" value="<?php echo $_POST['so1']; ?>" readonly /></td>
                </tr>
                <tr>
                    <td style="color: blue">Số thứ hai:</td>
                    <td><input type="text" name="so2" size="27" value="<?php echo $_POST['so2']; ?>" readonly /></td>
                </tr>
                <tr>
                    <td style="color: blue">Kết quả:</td>
                    <td><input type="text" size="27" readonly value="<?php echo isset($kq) ? $kq : ''; ?>" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td style="text-align: left">
                        <a href="javascript:window.history.back(-1);">Trở về trang trước</a>
                    </td>
                </tr>
            </table>
        </form>
    </section>
</body>

</html>