<?php

if (isset($_POST['submit'])) {
    $dai = $_POST['chieudai'];
    $rong = $_POST['chieurong'];
    if (!is_numeric($dai) or !is_numeric($rong)) $error = "Hãy nhập vào số";
    else  if ($dai <= 0 or $rong <= 0) $error = "Kích thước phải lớn hơn 0";
    else if ($dai < $rong) $error = "Chiều dài phải lớn hơn chiều rộng";
    else {
        $dientich = $dai * $rong;
        $chuvi = ($dai + $rong) * 2;
    }
}
?>

<form action="" method="POST">
    <table align="center" style="background-color: khaki; padding: 20px;">
        <tr>
            <td colspan="2">
                <h2 style="background-color: orange;">TÍNH HÌNH CHỮ NHẬT</h2>
            </td>
        </tr>
        <tr>
            <td>Chiều dài: </td>
            <td><input type="text" name="chieudai" required value="<?php if (isset($_POST['chieudai'])) echo $_POST['chieudai']; ?>"></td>

        </tr>
        <tr>
            <td>Chiều rộng: </td>
            <td><input type="text" name="chieurong" value="<?php if (isset($_POST['chieurong'])) echo $_POST['chieurong']; ?>" required></td>
        </tr>
        <tr>
            <td>Chu vi: </td>
            <td><input type="text" value="<?php if (isset($chuvi)) echo $chuvi; ?>" readonly></td>
        </tr>
        <tr>
            <td>Diện tích: </td>
            <td><input type="text" value="<?php if (isset($dientich)) echo $dientich; ?>" readonly></td>

        </tr>
        <tr>
            <td colspan="2" style="vertical-align: middle;"> <?php if (isset($error)) echo "<br><i style='color:red';>" . $error . "</i>"; ?></td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align: center;"><input type="submit" value="Tính" name="submit"></td>
        </tr>
    </table>
</form>