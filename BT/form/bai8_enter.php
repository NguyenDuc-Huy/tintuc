<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your Information</title>
</head>

<body>
    <h2>Enter Your Information</h2>
    <form action="bai8_confirm.php" method="POST">
        <table>
            <tr>
                <td>Full name</td>
                <td><input type="text" name="name" required></td>
            </tr>
            <tr>
                <td>Address</td>
                <td><input type="text" name="add" required></td>
            </tr>
            <tr>
                <td>Phone</td>
                <td><input type="text" name="phone" required></td>
            </tr>
            <tr>
                <td>Gender</td>
                <td><input type="radio" name="sex" checked value="Male"><label for="">Male</label>
                    <input type="radio" name="sex" value="Female"><label>Female</label>
                </td>
            </tr>
            <tr>
                <td>Country</td>
                <td>
                    <select name="country" id="">
                        <option value="Vietnam">Vietnam</option>
                        <option value="America">America</option>
                        <option value="China">China</option>
                        <option value="Rusia">Rusia</option>
                        <option value="France">France</option>
                        <option value="England">England</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Study</td>
                <td>
                    <input type="checkbox" name="study[]" value="PHP & MySql"> <label for="">PHP&MySql</label>
                    <input type="checkbox" name="study[]" value="ASP.NET"> <label for="">ASP.NET</label>
                    <input type="checkbox" name="study[]" value="CCNA"> <label for="">CCNA</label>
                    <input type="checkbox" name="study[]" value="Security+"> <label for="">Security+</label>
                </td>
            </tr>
            <tr>
                <td>Note</td>
                <td><textarea name="note" id="" cols="35" rows="6"></textarea></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" name="submit" value="Gửi"><input type="reset" value="Hủy"></td>
            </tr>
        </table>
    </form>
</body>

</html>