<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Tính toán trên 2 số</title>
  <style>
    section {
      position: absolute;
      left: 50%;
      top: 20%;
      transform: translate(-50%, -50%);
    }

    body {
      font-weight: bold;
    }

    label {
      color: red;
    }
  </style>

</head>

<body>
  <section>
    <h2 align="center">PHÉP TÍNH TRÊN HAI SỐ</h2>
    <form action="bai6_ketqua.php" method="POST">
      <table>
        <tr>
          <td style="color: rgb(168, 15, 15)">Chọn phép tính</td>
          <td>
            <input type="radio" name="pheptinh" value="Cộng" checked /><label for="">Cộng</label>
            <input type="radio" name="pheptinh" value="Trừ" /><label for="">Trừ</label>
            <input type="radio" name="pheptinh" value="Nhân" /><label for="">Nhân</label>
            <input type="radio" name="pheptinh" value="Chia" /><label for="">Chia</label>
          </td>
        </tr>
        <tr>
          <td style="color: blue">Số thứ nhất:</td>
          <td><input type="text" name="so1" size="27" required /></td>
        </tr>
        <tr>
          <td style="color: blue">Số thứ hai:</td>
          <td><input type="text" name="so2" size="27" required /></td>
        </tr>
        <tr>
          <td></td>
          <td style="text-align: left">
            <input type="submit" name=" submit" value="Tính" />
          </td>
        </tr>
      </table>
    </form>
  </section>
</body>

</html>