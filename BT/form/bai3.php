<?php

if (isset($_POST['submit'])) {
    $csc = $_POST['csc'];
    $csm = $_POST['csm'];
    $dg = $_POST['dg'];
    if (!is_numeric($csc) or !is_numeric($csm) or !is_numeric($dg)) $error = "Yêu cầu nhập vào dạng số";
    else if ($csc < 0 or $csm < 0) $error = "Chỉ số phải lớn hơn hoặc bằng 0";
    else if ($dg < 0)  $error = "Đơn giá phải lớn hơn 0";
    else if ($csm < $csc) $error = "Chỉ số mới phải >= chỉ số cũ";
    else {
        $tien = ($csm - $csc) * $dg;
    }
}
?>

<form action="" method="POST">
    <table align="center" style="background-color: khaki; padding: 20px;">
        <tr>
            <td colspan="2">
                <h2 style="background-color: orange;">THANH TOÁN TIỀN ĐIỆN</h2>
            </td>
        </tr>
        <tr>
            <td>Tên chủ hộ: </td>
            <td><input type="text" name="ten" required value="<?php if (isset($_POST['ten'])) echo $_POST['ten']; ?>"></td>

        </tr>
        <tr>
            <td>Chỉ số cũ: </td>
            <td><input type="text" name="csc" required value="<?php if (isset($_POST['csc'])) echo $_POST['csc']; ?>" required><label for="">(kWh)</label></td>
        </tr>
        <tr>
            <td>Chỉ số mới: </td>
            <td><input type="text" name="csm" required value="<?php if (isset($_POST['csm'])) echo $_POST['csm']; ?>" required><label for="">(kWh)</label></td>
        </tr>
        <tr>
            <td>Đơn giá: </td>
            <td><input type="text" name="dg" required value="<?php if (isset($_POST['dg'])) echo $_POST['dg'];
                                                                else echo "2000" ?>"><label for="">(VND)</label></td>
        </tr>
        <tr>
            <td>Số tiền thanh toán: </td>
            <td><input type="text" value="<?php if (isset($tien)) echo $tien; ?>" readonly><label for="">(VND)</label></td>
        </tr>

        <tr>
            <td colspan="2" style="vertical-align: middle;"> <?php if (isset($error)) echo "<br><i style='color:red';>" . $error . "</i>"; ?></td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align: center;"><input type="submit" value="Tính" name="submit"></td>
        </tr>
    </table>
</form>