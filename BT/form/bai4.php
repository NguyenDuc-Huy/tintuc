<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kết quả thi đại học</title>
</head>

<body>
    <?php
    if (isset($_POST['submit'])) {
        $dtoan = $_POST['dtoan'];
        $dly = $_POST['dly'];
        $dhoa = $_POST['dhoa'];
        if (!is_numeric($dtoan) or !is_numeric($dly) or !is_numeric($dhoa))
            $error = "Điểm nhập vào phải dạng số";
        else if ($dtoan < 0 or $dly < 0 or $dhoa < 0 or $dtoan > 10 or $dly > 10 or $dhoa > 10)
            $error = "Điểm phải >=0 và <=10";
        else {
            $tong = $dtoan + $dly + $dhoa;
            if ($dtoan * $dly * $dhoa != 0 and $tong >= $_POST['diemchuan'])
                $kq = "Đậu";
            else $kq = "Rớt";
        }
    }
    ?>

    <form action="" method="POST">
        <table align="center" style="background-color: lightpink; padding: 5px;">
            <tr>
                <td colspan="2" style="background-color: tomato; " align="center">
                    <h2>KẾT QUẢ THI ĐẠI HỌC</h2>
                </td>
            </tr>

            <tr>
                <td>Toán: </td>
                <td><input type="text" name="dtoan" required value="<?php if (isset($_POST['dtoan'])) echo $_POST['dtoan']; ?>"></td>

            </tr>
            <tr>
                <td>Lý: </td>
                <td><input type="text" name="dly" value="<?php if (isset($_POST['dly'])) echo $_POST['dly']; ?>" required></td>
            </tr>
            <tr>
                <td>Hóa: </td>
                <td><input type="text" name="dhoa" value="<?php if (isset($_POST['dhoa'])) echo $_POST['dhoa']; ?>" required></td>
            </tr>
            <tr>
                <td>Điểm chuẩn: </td>
                <td><input type="text" name="diemchuan" value="<?php if (isset($diemchuan)) echo $diemchuan;
                                                                else echo '20'; ?>"></td>
            </tr>
            <tr>
                <td>Tổng điểm thi: </td>
                <td><input type="text" value="<?php if (isset($tong)) echo $tong; ?>" readonly></td>

            </tr>
            <tr>
                <td>Kết quả thi: </td>
                <td><input type="text" value="<?php if (isset($kq)) echo $kq; ?>" readonly></td>

            </tr>
            <tr>
                <td colspan="2"> <?php if (isset($error)) echo "<br><i style='color:blue';>" . $error . "</i>"; ?></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align: center;"><input type="submit" value="Xem kết quả" name="submit"></td>
            </tr>
        </table>
    </form>
</body>

</html>