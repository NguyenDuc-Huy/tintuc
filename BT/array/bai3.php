<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Phát sinh mảng và tính toán</title>

</head>
<?php
function InMang($arr)
{
    foreach ($arr as $v) {
        echo $v . " ";
    }
}

function TimMax($arr)
{
    $max = $arr[0];
    for ($i = 1; $i < count($arr); $i++)
        if ($arr[$i] > $max)
            $max = $arr[$i];
    return $max;
}
function TimMin($arr)
{
    $min = $arr[0];
    for ($i = 1; $i < count($arr); $i++)
        if ($arr[$i] < $min)
            $min = $arr[$i];
    return $min;
}


if (isset($_POST['sopt'])) {
    if (is_int($_POST['sopt'] + 0) and is_numeric($_POST['sopt']) and $_POST['sopt'] > 0)
        for ($i = 0; $i < $_POST['sopt']; $i++)
            $arr[$i] = rand(0, 20);

    else $err = "<i style='color:red'>Nhập vào số nguyên dương</i>";
}


?>

<body>
    <form action="" method="POST">

        <table align="center" style="outline: 1px solid black;">
            <tr>
                <td colspan="2">
                    <h2 style="background-color: #A70F74; padding: 15px; margin:0; text-align: center;  color:white">PHÁT SINH MẢNG VÀ TÍNH TOÁN</h2>
                </td>
            </tr>
            <tr style="background-color: #FFDBF5;">
                <td>Nhập số phần tử: </td>
                <td><input type="text" name="sopt" required value="<?php if (isset($_POST['sopt'])) echo $_POST['sopt']; ?>"></td>

            </tr>
            <tr style="background-color: #FFDBF5;" <?php echo isset($err) ? "" : "hidden"; ?>>
                <td></td>
                <td><?php echo isset($err) ? $err : ""; ?></td>
            </tr>
            <tr style="background-color: #FFDBF5;">
                <td></td>
                <td style="text-align: left;"><input type="submit" value="Phát sinh và tính toán" name="submit"></td>
            </tr>

            <tr>
                <td>Mảng: </td>
                <td><input style="background-color: #FEA9A7;" type="text" name="mang" readonly value="<?php if (isset($arr)) InMang($arr) ?>" size="30"></td>

            </tr>
            <tr>
                <td>GTLN (Max) trong mảng: </td>
                <td><input style="background-color: #FEA9A7;" type="text" value="<?php if (isset($arr)) echo TimMax($arr) ?>" readonly></td>
            </tr>
            <tr>
                <td>GTNN (Min) trong mảng: </td>
                <td><input style="background-color: #FEA9A7;" type="text" value="<?php if (isset($arr)) echo TimMin($arr) ?>" readonly></td>
            </tr>
            <tr>
                <td>Tổng mảng: </td>
                <td><input style="background-color: #FEA9A7;" type="text" value="<?php if (isset($arr)) echo array_sum($arr); ?>" readonly></td>
            </tr>

            <tr>
                <td colspan="2" style="text-align:center">(<span style="color: red;">Ghi chú: </span>Các phần tử trong mảng có giá trị từ 0 đến 20)</td>
            </tr>

        </table>

    </form>
</body>

</html>