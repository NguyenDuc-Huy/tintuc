<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tính toán</title>

</head>
<?php
function count_evenNum($arr)
{
    $count = 0;
    foreach ($arr as $value) {
        if ($value % 2 == 0)
            $count++;
    }
    return $count > 0 ? "Có " . $count . " số chẵn trong mảng" : "Không có số chẵn trong mảng";
}
function count_less100($arr)
{
    $count = 0;
    foreach ($arr as $value) {
        if ($value < 100)
            $count++;
    }
    return $count > 0 ? "Có " . $count . " số bé hơn 100" : "Không có số bé hơn 100 trong mảng";
}
function count_negativeNum($arr)
{
    $count = 0;
    foreach ($arr as $value) {
        if ($value < 0)
            $count++;
    }
    return $count > 0 ? "Có " . $count . " số âm trong mảng" : "Không có giá trị âm trong mảng";
}
function zero_index($arr)
{
    $index = "";
    foreach ($arr as $key => $v) {
        if ($v == 0)
            $index .= $key . " ";
    }
    return strlen($index) > 0 ? "Vị trí các số 0 trong mảng: " . $index : "Không có số 0 trong mảng";
}
function quick_sort($arr)
{
    if (count($arr) <= 1) {
        return $arr;
    } else {
        $pivot = $arr[0];
        $left = array();
        $right = array();
        for ($i = 1; $i < count($arr); $i++) {
            if ($arr[$i] < $pivot) {
                $left[] = $arr[$i];
            } else {
                $right[] = $arr[$i];
            }
        }
        return array_merge(quick_sort($left), array($pivot), quick_sort($right));
    }
}
function inMang($arr)
{
    return implode(" ", $arr);
}

if (isset($_POST['submit']))
    if (!is_numeric($_POST['so_n']) or $_POST['so_n'] <= 0 or !is_int($_POST['so_n'] + 0))
        $error = "Nhập vào số nguyên dương";
    else {
        $arr = [];
        for ($i = 0; $i < $_POST['so_n']; $i++)
            $arr[] = rand(-100, 200);
    }
?>

<body>
    <form action="" method="POST">
        <table align="center" style="background-color: khaki;">
            <tr>
                <td colspan="2">
                    <h2 style="background-color: orange; padding: 15px; margin:0; text-align:center;">NHẬP VÀ TÍNH TOÁN</h2>
                </td>
            </tr>
            <tr>
                <td>Nhập số n: </td>
                <td><input type="text" name="so_n" required value="<?php if (isset($_POST['so_n'])) echo $_POST['so_n']; ?>"> <span style="color: red;">(*)</span></td>

            </tr>
            <tr>
                <td></td>
                <td style="text-align: left;"><input type="submit" value="Tính" name="submit"></td>
            </tr>
            <tr <?php echo isset($error) ? "" : "hidden"; ?>>
                <td></td>
                <td><?php if (isset($error)) echo $error; ?></td>
            </tr>
            <tr>
                <td>Kết quả: </td>
                <td><textarea cols="30" rows="8"><?php if (isset($arr)) {
                                                        echo inMang($arr) . '&#010';
                                                        echo count_evenNum($arr) . '&#010';
                                                        echo count_less100($arr) . '&#010';
                                                        echo count_negativeNum($arr) . '&#010';
                                                        echo zero_index($arr) . '&#010';
                                                        $arr = quick_sort($arr);
                                                        echo inMang($arr);
                                                    } ?></textarea></td>
            </tr>
        </table>
    </form>
</body>

</html>