<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Phát sinh mảng và tính toán</title>

</head>
<?php
if (isset($_POST['nam'])) {
    $nam = $_POST['nam'];
    if (is_numeric($nam) and $nam > 0 and is_int($nam + 0)) {
        $mang_can = array('Canh', 'Tân', 'Nhâm', 'Quý', 'Giáp', 'Ất', 'Bính', 'Đinh', 'Mậu', 'Kỷ');
        $mang_chi = array('thân', 'dậu', 'tuất', 'hợi', 'tí', 'sửu', 'dần', 'mão', 'thìn', 'tỵ', 'ngọ', 'mùi');
        $mang_hinh = array('than.png', 'dau.png', 'tuat.png', 'hoi.png', 'ti.png', 'suu.png', 'dan.png', 'mao.png', 'thin.png', 'ty.png', 'ngo.png', 'mui.png');
        $can = $nam % 10;
        $chi = $nam % 12;
        $nam_AL = $mang_can[$can] . " " . $mang_chi[$chi];
        $hinh = $mang_hinh[$chi];
    } else $err = "Nhập lại năm dương lịch";
}

?>

<body>
    <form action="" method="POST">
        <table align="center" style="background-color:#CCD9CF;">
            <tr>
                <td colspan="3">
                    <h2 style="background-color: orange; padding: 15px; margin:0; text-align: center;">TÍNH NĂM ÂM LỊCH</h2>
                </td>
            </tr>
            <tr style="text-align: center;">
                <td>Năm dương lịch </td>
                <td></td>
                <td>Năm âm lịch</td>
            </tr>
            <tr style="text-align: center;">
                <td><input type="text" name="nam" required value="<?php if (isset($_POST['nam'])) echo $_POST['nam']; ?>" size="10"></td>
                <td style="text-align: center;"><input type="submit" name="subnit" value="=>"></td>
                <td><input type="text" readonly value="<?php if (isset($nam_AL)) echo $nam_AL; ?>" size="10"></td>
            </tr>

            <tr <?php if (isset($err)) echo "";
                else echo "hidden"; ?>>

                <td colspan="3"><?php if (isset($err)) echo $err; ?>
                </td>
            </tr>

            <tr>
                <td colspan="3" style="text-align: center;"><?php if (isset($hinh)) echo "<img width='300' height='300' src='./asset/img/$hinh' alt='Hinh linh vat'>"; ?>
                </td>
            </tr>

        </table>
    </form>
</body>

</html>