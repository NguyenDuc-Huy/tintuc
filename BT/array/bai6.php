<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tính dãy số</title>

</head>
<?php

if (isset($_POST['dayso'])) {
    $arr = explode(',', $_POST['dayso']);
}
function InMang($arr)
{
    echo implode(" ", $arr);
}

?>
<style>
    span {
        color: red;
    }
</style>

<body>
    <form action="" method="POST">
        <table align="center" style="background-color: #CCD9CF;">
            <tr>
                <td colspan="2">
                    <h2 style="background-color:lightseagreen; padding: 15px; margin:0; text-align: center; color: white;">SẮP XẾP MẢNG</h2>
                </td>
            </tr>
            <tr>
                <td>Nhập mảng: </td>
                <td><input type="text" name="dayso" required value="<?php if (isset($_POST['dayso'])) echo $_POST['dayso']; ?>" size="30"><span> (*)</span></td>

            </tr>

            <tr>
                <td></td>
                <td style="text-align: left; "><input style=" padding: 3px 5px;" type="submit" value="Sắp xếp tăng/giảm" name="submit"></td>
            </tr>
            <tr>
                <td><span>Mảng sau khi sắp xếp:</span> </td>
                <td></td>
            </tr>
            <tr>
                <td>Tăng dần </td>
                <td><input style="background-color: powderblue;" type="text" value="<?php if (isset($arr)) {
                                                                                        sort($arr);
                                                                                        InMang($arr);
                                                                                    }; ?>" readonly size="30"></td>
            </tr>
            <tr>
                <td>Giảm dần </td>
                <td><input style="background-color: powderblue;" type="text" value="<?php if (isset($arr)) {
                                                                                        rsort($arr);
                                                                                        InMang($arr);
                                                                                    } ?>" readonly size="30"></td>
            </tr>

            <tr style="background-color: #71D3CE;">
                <td colspan="2" style="text-align:center"><span style="color: red;">(*) </span>Các phần tử của mảng được nhập cách nhau bởi dấu ","</td>
            </tr>

        </table>
    </form>
</body>

</html>