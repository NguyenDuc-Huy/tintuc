<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tính dãy số</title>

</head>
<?php
if (isset($_POST['dayso'])) {
    $tong = array_sum(explode(',', trim($_POST['dayso'])));
}
?>

<body>
    <form action="" method="POST">
        <table align="center" style="background-color: #CCD9CF;">
            <tr>
                <td colspan="2">
                    <h2 style="background-color: lightseagreen; padding: 15px; margin:0;color:white;">NHẬP VÀ TÍNH TRÊN DÃY SỐ</h2>
                </td>
            </tr>
            <tr>
                <td>Nhập dãy số: </td>
                <td><input type="text" name="dayso" required value="<?php if (isset($_POST['dayso'])) echo $_POST['dayso']; ?>"> <span style="color: red;">(*)</span></td>

            </tr>
            <tr>
                <td></td>
                <td style="text-align: left;"><input type="submit" value="Tổng dãy số" name="submit"></td>
            </tr>
            <tr>
                <td>Tổng dãy số: </td>
                <td><input type="text" value="<?php if (isset($tong)) echo $tong; ?>" readonly></td>
            </tr>

            <tr>
                <td colspan="2" style="text-align:center"><span style="color: red;">(*) </span>Các số được nhập cách nhau bởi dấu ","</td>
            </tr>

        </table>
    </form>
</body>

</html>