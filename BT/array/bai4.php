<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tính dãy số</title>

</head>
<?php

if (isset($_POST['dayso'])) {
    $arr = explode(',', $_POST['dayso']);
}
function InMang($arr)
{
    $mang = implode(" ", $arr);
    echo $mang;
}
function TimKiem($arr, $n)
{
    foreach ($arr as $key => $value) {
        if ($value == $n) {
            return "Tìm thấy $n tại vị trí $key trong mảng.";
        }
    }
    return "Không tìm thấy $n trong mảng";
}
?>

<body>
    <form action="" method="POST">
        <table align="center" style="background-color: #CCD9CF;">
            <tr>
                <td colspan="2">
                    <h2 style="background-color:lightseagreen; padding: 15px; margin:0; text-align: center; color: white;">TÌM KIẾM</h2>
                </td>
            </tr>
            <tr>
                <td>Nhập mảng: </td>
                <td><input type="text" name="dayso" required value="<?php if (isset($_POST['dayso'])) echo $_POST['dayso']; ?>" size="30"></td>

            </tr>
            <tr>
                <td>Nhập số cần tìm: </td>
                <td><input type="text" name="so" required value="<?php if (isset($_POST['so'])) echo $_POST['so']; ?>"></td>

            </tr>
            <tr>
                <td></td>
                <td style="text-align: left; "><input style="background-color: skyblue; padding: 3px 5px;" type="submit" value="Tìm kiếm" name="submit"></td>
            </tr>
            <tr>
                <td>Mảng: </td>
                <td><input type="text" value="<?php if (isset($arr)) InMang($arr); ?>" readonly size="30"></td>
            </tr>
            <tr>
                <td>Kết quả tìm kiếm: </td>
                <td><input style="background-color: powderblue;" type="text" value="<?php if (isset($arr)) echo TimKiem($arr, $_POST['so']); ?>" readonly size="30"></td>
            </tr>

            <tr style="background-color: #71D3CE;">
                <td colspan="2" style="text-align:center"><span style="color: red;">(*) </span>Các phần tử của mảng được nhập cách nhau bởi dấu ","</td>
            </tr>

        </table>
    </form>
</body>

</html>