<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tính dãy số</title>

</head>
<?php

if (isset($_POST['dayso'])) {
    $dayso = ($_POST['dayso']);
    $arr = explode(',', $dayso);
}
function InMang($arr)
{
    echo implode(" ", $arr);
}
function ThayThe(&$arr, $old, $new)
{
    for ($i = 0; $i < count($arr); $i++)
        if ($arr[$i] == $old)
            $arr[$i] = " " . $new;
}
?>

<body>
    <form action="" method="POST">
        <table align="center" style="outline: 1px solid grey;">
            <tr>
                <td colspan="2">
                    <h2 style="background-color: #A70F74; padding: 15px; margin:0;text-align:center; color: white;">THAY THẾ</h2>
                </td>
            </tr>
            <tr style="background-color: #FFDBF5;">
                <td>Nhập các phần tử: </td>
                <td><input type="text" name="dayso" required value="<?php if (isset($_POST['dayso'])) echo $_POST['dayso']; ?>" size="30"></td>

            </tr>
            <tr style="background-color: #FFDBF5;">
                <td>Giá trị thay thế: </td>
                <td><input type="text" name="gtc" required value="<?php if (isset($_POST['gtc'])) echo $_POST['gtc']; ?>"></td>

            </tr>
            <tr style="background-color: #FFDBF5;">
                <td>Giá trị cần thay thế: </td>
                <td><input type="text" name="gtm" required value="<?php if (isset($_POST['gtm'])) echo $_POST['gtm']; ?>"></td>

            </tr>
            <tr style="background-color: #FFDBF5;">
                <td></td>
                <td style="text-align: left;"><input style="background-color: lightyellow;" type="submit" value="Thay thế" name="submit"></td>
            </tr>
            <tr>
                <td>Mảng cũ: </td>
                <td><input style="background-color: #FEA9A7; " type=" text" value="<?php if (isset($arr)) InMang($arr); ?>" readonly size="30"></td>
            </tr>
            <tr>
                <td>Mảng sau khi thay thế: </td>
                <td><input style="background-color: #FEA9A7;" type=" text" value="<?php if (isset($arr)) {
                                                                                        ThayThe($arr, $_POST['gtc'], $_POST['gtm']);
                                                                                        InMang($arr);
                                                                                    }; ?>" readonly size="30"></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center"><span style="color: red;">Ghi chú: </span>Các phần tử của mảng được nhập cách nhau bởi dấu ","</td>
            </tr>

        </table>
    </form>
</body>

</html>