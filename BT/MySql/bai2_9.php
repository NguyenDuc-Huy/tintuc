<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Thông tin các sản phẩm</title>

</head>

<body>

    <?php
    require('config.php');
    //$conn = mysqli_connect('localhost', 'root', '', 'quanly_ban_sua')
    $conn = mysqli_connect($hostname, $username, $password, $dbname) or die('Không thể kết nối tới database' . mysqli_connect_error());
    mysqli_set_charset($conn, 'utf8');
    //phan trang



    if (isset($_POST['tensua'])) {
        $tensua = $_POST['tensua'];

        // $rowsPerPage = 2;
        // if (!isset($_GET['page'])) {
        //     $_GET['page'] = 1;
        // }
        // $offset = ($_GET['page'] - 1) * $rowsPerPage;

        $query = "Select * from sua where Ten_sua like '%$tensua%'";
        $result = mysqli_query($conn, $query);
        $numRows = mysqli_num_rows($result);

        // $maxPage = ceil($numRows / $rowsPerPage);
        // $query2 = mysqli_query($conn, "Select * from sua where Ten_sua like '%$tensua%'");
        // $num_result = mysqli_num_rows($query2);
    }

    ?>
    <form action="" method="POST" style="margin-bottom: 15px;">
        <table align="center" style="background-color: #FECCCD; padding:5px; width:80%">
            <tr align="center">
                <td>
                    <h2 style="margin: 5px;">TÌM KIẾM THÔNG TIN SỮA</h2>
                </td>
            </tr>
            <tr align="center">
                <td>Tên sữa
                    <input type="text" name="tensua" value="<?php if (isset($tensua)) echo $tensua; ?>" size="30">&nbsp;&nbsp;
                    <input type="submit" name="submit" value="Tìm kiếm">
                </td>

            </tr>
        </table>
    </form>
    <?php
    if (isset($numRows) and $numRows <> 0) {
    ?>

        <p style="text-align: center;">Có <?php echo $numRows; ?> sản phẩm được tìm thấy</p>
        <table border="1" cellpadding="5" cellspacing="0" align="center" style="width:50%; outline:solid 1px indianred">
            <?php
            while ($row = $result->fetch_assoc()) {


            ?>
                <tr>
                    <td colspan="2" align="center" bgcolor="#FFEEE6">
                        <?php echo  '<span style="font-size:25px; font-weight:bold; color:orange;">' . $row['Ten_sua'] . '</span>';
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php
                        echo '<image src="./Hinh_sua/' . $row['Hinh'] . '" alt="hinh sua" width="150" height="150"/>';
                        ?>
                    </td>
                    <td>
                        <p style="font-weight: bold; font-style:italic">Thành phần sinh dưỡng:</p>
                        <?php
                        echo '<p>' . $row['TP_Dinh_Duong'] . '</p>'
                        ?>
                        <p style="font-weight: bold; font-style:italic">Lợi ích:</p>
                        <?php
                        echo '<p>' . $row['Loi_ich'] . '</p>';
                        echo '<p style="text-align: right;"><b><i>Trọng lượng: </i></b>' . $row['Trong_luong'] . ' - <b><i>Đơn giá: </i></b>' . $row['Don_gia'] . '</p>';
                        ?>

                    </td>
                </tr>
            <?php
            }
            ?>
        </table>
    <?php } ?>

</body>

</html>