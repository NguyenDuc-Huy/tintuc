<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Thông tin các sản phẩm</title>

</head>

<body>

    <?php
    require('config.php');
    //$conn = mysqli_connect('localhost', 'root', '', 'quanly_ban_sua')
    $conn = mysqli_connect($hostname, $username, $password, $dbname) or die('Không thể kết nối tới database' . mysqli_connect_error());
    mysqli_set_charset($conn, 'utf8');
    //phan trang
    $rowsPerPage = 11;
    if (!isset($_GET['page'])) {
        $_GET['page'] = 1;
    }
    $offset = ($_GET['page'] - 1) * $rowsPerPage;

    $query = "Select * from sua LIMIT $offset, $rowsPerPage";
    $result = mysqli_query($conn, $query);
    $numRows = mysqli_num_rows($result);
    $maxPage = ceil($numRows / $rowsPerPage);

    if ($numRows <> 0) {
    ?>

        <table align='center' border='1' cellpadding='2' cellspacing='2' style='border-collapse:collapse'>
            <tr>
                <th style="font-weight: bold; background-color: thistle; color: orangered;" colspan="5">THÔNG TIN CÁC SẢN PHẨM</th>
            </tr>

            <tr>
                <?php
                $dem = 0;
                while ($rows = mysqli_fetch_array($result)) {
                    $dem++;
                    if ($dem > 5) break;

                ?>
                    <td align="center" style="width:260px">
                        <?php
                        echo '<p style="font-weight:bold; ">' . $rows["Ten_sua"] . '</p>';
                        echo '<p style="margin-top:-10px">' . $rows['Trong_luong'] . "g - " . $rows['Don_gia'] . " VND</p>";
                        echo '<img src="./Hinh_sua/' . $rows['Hinh'] . '" alt="hinh sua" width="100" height="100" >'
                        ?>
                    </td>
                <?php } ?>
            </tr>
            <tr>
                <?php
                while ($rows = mysqli_fetch_array($result)) {
                ?>
                    <td align="center" style="width:260px">
                        <?php
                        echo '<p style="font-weight:bold; ">' . $rows["Ten_sua"] . '</p>';
                        echo '<p style="margin-top:-10px">' . $rows['Trong_luong'] . "g - " . $rows['Don_gia'] . " VND</p>";
                        echo '<img src="./Hinh_sua/' . $rows['Hinh'] . '" alt="hinh sua" width="100" height="100" >'
                        ?>
                    </td>
                <?php } ?>
            </tr>
        </table>
    <?php
        echo "<p style='text-align:center; font-size:18px'>";
        $re = mysqli_query($conn, 'select * from sua');
        $numRows = mysqli_num_rows($re);
        $maxPage = floor($numRows / $rowsPerPage) + 1;
        if ($_GET['page'] > 1) {
            echo "<a href=" . $_SERVER['PHP_SELF'] . "?page=1"  . "><<&nbsp</a> ";
            echo "<a href=" . $_SERVER['PHP_SELF'] . "?page=" . ($_GET['page'] - 1) . "><</a> "; //gắn thêm nút Back
        }
        for ($i = 1; $i <= $maxPage; $i++) {
            if ($i == $_GET['page']) {
                echo '<b>' . $i . '</b> '; //trang hiện tại sẽ được bôi đậm
            } else echo "<a href=" . $_SERVER['PHP_SELF'] . "?page=" . $i . "> " . $i . "</a> ";
        }
        if ($_GET['page'] < $maxPage) {
            echo "<a href=" . $_SERVER['PHP_SELF'] . "?page=" . ($_GET['page'] + 1) . ">></a> ";
            echo "<a href=" . $_SERVER['PHP_SELF'] . "?page=$maxPage"  . ">&nbsp>></a>";  //gắn thêm nút Next
        }
        echo "</p>";
        //    echo 'Tong so trang la: '.$maxPage;

    }
    mysqli_close($conn);
    ?>
</body>

</html>