<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Thông tin các sản phẩm</title>

</head>

<body>

    <?php
    require('config.php');
    //$conn = mysqli_connect('localhost', 'root', '', 'quanly_ban_sua')
    $conn = mysqli_connect($hostname, $username, $password, $dbname) or die('Không thể kết nối tới database' . mysqli_connect_error());
    mysqli_set_charset($conn, 'utf8');
    //phan trang
    $rowsPerPage = 2;
    if (!isset($_GET['page'])) {
        $_GET['page'] = 1;
    }
    $offset = ($_GET['page'] - 1) * $rowsPerPage;

    $query = "Select * from sua LIMIT $offset, $rowsPerPage";
    $result = mysqli_query($conn, $query);
    $numRows = mysqli_num_rows($result);
    $maxPage = ceil($numRows / $rowsPerPage);

    if ($numRows <> 0) {
    ?>

        <table border="1" cellpadding="5" cellspacing="0" align="center" style="width:50%; outline:solid 1px indianred">
            <?php
            while ($row = $result->fetch_assoc()) {


            ?>
                <tr>
                    <td colspan="2" align="center" bgcolor="#FFEEE6">
                        <?php echo  '<span style="font-size:25px; font-weight:bold; color:orange;">' . $row['Ten_sua'] . '</span>';
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php
                        echo '<image src="./Hinh_sua/' . $row['Hinh'] . '" alt="hinh sua" width="180" height="180"/>';
                        ?>
                    </td>
                    <td>
                        <p style="font-weight: bold; font-style:italic">Thành phần sinh dưỡng:</p>
                        <?php
                        echo '<p>' . $row['TP_Dinh_Duong'] . '</p>'
                        ?>
                        <p style="font-weight: bold; font-style:italic">Lợi ích:</p>
                        <?php
                        echo '<p>' . $row['Loi_ich'] . '</p>';
                        echo '<p style="text-align: right;"><b><i>Trọng lượng: </i></b>' . $row['Trong_luong'] . ' - <b><i>Đơn giá: </i></b>' . $row['Don_gia'] . '</p>';
                        ?>

                    </td>
                </tr>
            <?php
            }

            ?>

        </table>
    <?php
        echo "<p style='text-align:center; font-size:18px'>";
        $re = mysqli_query($conn, 'select * from sua');
        $numRows = mysqli_num_rows($re);
        $maxPage = floor($numRows / $rowsPerPage) + 1;
        if ($_GET['page'] > 1) {
            echo "<a href=" . $_SERVER['PHP_SELF'] . "?page=1"  . "><<&nbsp</a> ";
            echo "<a href=" . $_SERVER['PHP_SELF'] . "?page=" . ($_GET['page'] - 1) . "><</a> "; //gắn thêm nút Back
        }
        for ($i = 1; $i <= $maxPage; $i++) {
            if ($i == $_GET['page']) {
                echo '<b>' . $i . '</b> '; //trang hiện tại sẽ được bôi đậm
            } else echo "<a href=" . $_SERVER['PHP_SELF'] . "?page=" . $i . "> " . $i . "</a> ";
        }
        if ($_GET['page'] < $maxPage) {
            echo "<a href=" . $_SERVER['PHP_SELF'] . "?page=" . ($_GET['page'] + 1) . ">></a> ";
            echo "<a href=" . $_SERVER['PHP_SELF'] . "?page=$maxPage"  . ">&nbsp>></a>";  //gắn thêm nút Next
        }
        echo "</p>";
        //    echo 'Tong so trang la: '.$maxPage;

    }
    mysqli_close($conn);
    ?>
</body>

</html>