<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thông tin nhân viên</title>
</head>

<body>
    <?php require 'config.php';
    $conn = new mysqli($hostname, $username, $password, $dbname);

    if ($conn->connect_errno) {
        echo "Lỗi kết nối database " . $conn->connect_errno;
    }
    ?>

    <h3 style="text-align: center;">THÔNG TIN KHÁCH HÀNG</h3>
    <table align="center" border="1" cellpadding="5" cellspacing="0">
        <tr style="text-align: center; color: red; font-weight: bold;">
            <td>Mã KH</td>
            <td>Tên khách hàng</td>
            <td>Giới tính</td>
            <td>Địa chỉ</td>
            <td>Số điện thoại</td>

        </tr>

        <?php

        $dem = 0;
        $query = "select * from khach_hang";
        $result = $conn->query($query);
        if (!$result) echo "Query failed " . $conn->error;
        while ($row = $result->fetch_array()) {
            $dem++;
            if ($dem % 2 == 1) echo '<tr style="background-color:lightsalmon";>';
            else echo "<tr>";
            echo "<td>" . $row['Ma_khach_hang'] . "</td>";
            echo "<td>" . $row['Ten_khach_hang'] . "</td>";
            if ($row['Phai'] == 0)
                echo "<td align='center'><img src='nam.jpg' height='30' width='30'></td>";
            else
                echo "<td align='center'><img src='nu.jpg' height='30' width='30'></td>";


            echo "<td>" . $row['Dia_chi'] . "</td>";
            echo "<td>" . $row['Dien_thoai'] . "</td>";


            echo "</tr>";
        }
        ?>
    </table>

</body>

</html>