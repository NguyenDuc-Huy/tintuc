﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thông tin hãng sữa</title>
</head>

<body>
    <?php require 'config.php';
    $conn = new mysqli($hostname, $username, $password, $dbname);

    if ($conn->connect_error) {
        echo "Lỗi kết nối database " . $conn->connect_error;
    }
    ?>

    <h3 style="text-align: center;">THÔNG TIN HÃNG SỮA</h3>
    <table align="center" border="1" cellpadding="5" cellspacing="0">
        <tr style="text-align: center; color: red;">
            <td>Mã HS</td>
            <td>Tên hãng sữa</td>
            <td>Địa chỉ</td>
            <td>Điện thoại</td>
            <td>Email</td>
        </tr>

        <?php
        $mau = 'pink';
        $dem = 0;
        $query = "select * from hang_sua";
        $result = $conn->query($query);
        if (!$result) echo "Query failed " . $conn->error;
        while ($row = $result->fetch_array()) {
            $dem++;
            echo "<tr>";
            echo "<td>" . $row['Ma_hang_sua'] . "</td>";
            echo "<td>" . $row['Ten_hang_sua'] . "</td>";
            echo "<td>" . $row['Dia_chi'] . "</td>";
            echo "<td>" . $row['Dien_thoai'] . "</td>";
            echo "<td>" . $row['Email'] . "</td>";


            echo "</tr>";
        }
        ?>
    </table>

</body>

</html>