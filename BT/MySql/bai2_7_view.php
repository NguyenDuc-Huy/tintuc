<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xem chi tiết sản phẩm</title>
</head>
<?php
require('config.php');

$conn = new mysqli($hostname, $username, $password, $dbname) or die('Không thể kết nối tới database' . mysqli_connect_error());
mysqli_set_charset($conn, 'utf8');

if (!isset($_GET['id']) || $_GET['id'] == NULL) {
    echo "<script>window.location= 'bai2_7.php' </script>";
} else {
    $id = $_GET['id'];
}
$query = "Select * from sua where Ma_sua = '$id'";
$result = $conn->query($query);
if ($result)
    $sua = $result->fetch_assoc();

?>

<body>
    <table border="1" cellpadding="5" cellspacing="0" align="center" style="width:50%; outline:solid 1px indianred">
        <tr>
            <td colspan="2" align="center" bgcolor="#FFEEE6">
                <?php echo  '<span style="font-size:25px; font-weight:bold; color:orange;">' . $sua['Ten_sua'] . '</span>';
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo '<image src="./Hinh_sua/' . $sua['Hinh'] . '" alt="hinh sua" />';
                ?>
            </td>
            <td>
                <p style="font-weight: bold; font-style:italic">Thành phần sinh dưỡng:</p>
                <?php
                echo '<p>' . $sua['TP_Dinh_Duong'] . '</p>'
                ?>
                <p style="font-weight: bold; font-style:italic">Lợi ích:</p>
                <?php
                echo '<p>' . $sua['Loi_ich'] . '</p>';
                echo '<p style="text-align: right;"><b><i>Trọng lượng: </i></b>' . $sua['Trong_luong'] . ' - <b><i>Đơn giá: </i></b>' . $sua['Don_gia'] . '</p>';
                ?>

            </td>
        </tr>
        <tr>
            <td align="right"><a href="javascript:window.history.back(-1);">Quay về</a></td>
            <td></td>
        </tr>
    </table>
</body>

</html>