<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Show data</title>
</head>

<body>
    <table align="center" header=true border=1>
        <tr>
            <th>Số hóa đơn</th>
            <th>Mã sữa</th>
            <th>Số lượng</th>
            <th>Đơn giá</th>
        </tr>

        <?php
        require("./config.php");
        $conn = new mysqli($hostname, $username, $password, $dbname);

        if ($conn->connect_errno) {
            echo "Lỗi kết nối database " . $conn->connect_errno;
        }
        //$query = "select * from ct_hoadon where Don_gia>=50000 and Ma_sua like 'VN%'";
        $query = "select * from ct_hoadon";

        $result = $conn->query($query);
        if (!$result) echo "Cau truy van bi sai";

        if ($result->num_rows != 0) {
            while ($row = $result->fetch_array()) { //row la 1 hang
                if ($row['Don_gia'] >= 50000 and substr($row['Ma_sua'], 0, 2) == 'VN') {
                    echo "<tr>";
                    echo "<td>" . $row['So_hoa_don'] . "</td>";
                    echo "<td>" . $row['Ma_sua'] . "</td>";
                    echo "<td>" . $row['So_luong'] . "</td>";
                    echo "<td>" . $row['Don_gia'] . "</td>";
                    echo "</tr>";
                }
            }
        } else "Bang khong co du lieu";

        $conn->close();
        ?>
    </table>
</body>

</html>