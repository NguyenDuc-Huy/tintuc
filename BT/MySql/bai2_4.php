<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Thông tin khách hàng</title>
</head>

<body>
    <?php
    require('config.php');
    //$conn = mysqli_connect('localhost', 'root', '', 'quanly_ban_sua')
    $conn = mysqli_connect($hostname, $username, $password, $dbname) or die('Không thể kết nối tới database' . mysqli_connect_error());
    mysqli_set_charset($conn, 'utf8');
    //phan trang
    $rowsPerPage = 5;
    if (!isset($_GET['page'])) {
        $_GET['page'] = 1;
    }
    $offset = ($_GET['page'] - 1) * $rowsPerPage;

    $query = "Select * from sua LIMIT $offset, $rowsPerPage";
    $result = mysqli_query($conn, $query);
    $numRows = mysqli_num_rows($result);
    $maxPage = ceil($numRows / $rowsPerPage);

    //print_r(mysqli_fetch_array($result));
    if ($numRows <> 0) {
    ?>
        <p align='center'>
            <font size='5' color='blue'> THÔNG TIN SỮA</font>
        </p>
        <table align='center' width='100%' border='1' cellpadding='2' cellspacing='2' style='border-collapse:collapse'>
            <tr>
                <th>Số TT</th>
                <th>Tên Sữa</th>
                <th>Hãng Sữa</th>
                <th>Loại Sữa</th>
                <th>Trọng Lượng</th>
                <th>Đơn giá</th>

            </tr>

            <?php
            $bg = '#eeeeee';
            $bg2 = 'pink';
            $sel = 0;

            while ($rows = mysqli_fetch_array($result)) {
                $sel++;
            ?>

                <tr bgcolor='<?php if ($sel % 2 == 0) echo $bg;
                                else echo $bg2; ?>'>
                    <td><?php echo $sel ?></td>
                    <td><?php echo $rows['Ten_sua'] ?></td>
                    <td><?php
                        $mhs = $rows['Ma_hang_sua'];
                        $query2 = "select * from hang_sua where Ma_hang_sua = '$mhs' ";
                        $result2 = $conn->query($query2);
                        $row2 = $result2->fetch_array();
                        echo $row2['Ten_hang_sua'];
                        ?></td>
                    <td><?php
                        $mls = $rows['Ma_loai_sua'];
                        $query2 = "select * from loai_sua where Ma_loai_sua = '$mls' ";
                        $result2 = $conn->query($query2);
                        $row2 = $result2->fetch_array();
                        echo $row2['Ten_loai'];
                        ?></td>
                    <td><?php echo $rows['Trong_luong'] ?></td>
                    <td><?php echo $rows['Don_gia'] ?></td>
                </tr>
            <?php } ?>
        </table>
    <?php
        echo "<p style='text-align:center; font-size:18px'>";
        $re = mysqli_query($conn, 'select * from sua');
        $numRows = mysqli_num_rows($re);
        $maxPage = floor($numRows / $rowsPerPage) + 1;
        if ($_GET['page'] > 1) {
            echo "<a href=" . $_SERVER['PHP_SELF'] . "?page=1"  . "><<&nbsp</a> ";
            echo "<a href=" . $_SERVER['PHP_SELF'] . "?page=" . ($_GET['page'] - 1) . "><</a> "; //gắn thêm nút Back
        }
        for ($i = 1; $i <= $maxPage; $i++) {
            if ($i == $_GET['page']) {
                echo '<b>' . $i . '</b> '; //trang hiện tại sẽ được bôi đậm
            } else echo "<a href=" . $_SERVER['PHP_SELF'] . "?page=" . $i . "> " . $i . "</a> ";
        }
        if ($_GET['page'] < $maxPage) {
            echo "<a href=" . $_SERVER['PHP_SELF'] . "?page=" . ($_GET['page'] + 1) . ">></a> ";
            echo "<a href=" . $_SERVER['PHP_SELF'] . "?page=$maxPage"  . ">&nbsp>></a>";  //gắn thêm nút Next
        }
        echo "</p>";
        //    echo 'Tong so trang la: '.$maxPage;

    }
    mysqli_close($conn);
    ?>
</body>

</html>