<?php
include './includes/header.php';
?>
<?php

?>
<section id="contentSection">
  <div class="row">
    <div class="col-sm-4">
      <?php
      $tentl = $theloai->getTL($_GET['theloaiId']);
      if ($tentl) {
        $res_ten = $tentl->fetch_assoc();

      ?>
        <h2 class="text-uppercase"><?php echo $res_ten['tentheloai'] ?></h2>

      <?php } ?>
    </div>
    <div class="col-sm-8 text-right" style="margin-top:15px">
      <form class="form-inline" method="POST" action="timkiem">
        <div class="form-group">
          <div class="input-group">
            <input type="text" class="form-control" size="30" placeholder="Nhập từ khóa tìm kiếm">
            <div class="input-group-addon"><button type="submit" name="submit" style="border: none; "><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="col-sm-12">
      <div id="menu_loaitin">
        <ul>
          <?php

          $tenlt = $loaitin->getLTTL($_GET['theloaiId']);
          if ($tenlt) {
            while ($tenlt_e = $tenlt->fetch_assoc()) {
              echo '<li><a href="loaitinpage.php?loaitinId=' . $tenlt_e['loaitinId']  . '">' . $tenlt_e['tenloaitin'] . '</a></li>';
            }
          }
          ?>
        </ul>
        <hr style="margin-top:0">
      </div>
    </div>


    <div class="col-lg-8 col-md-8 col-sm-8">

      <div class="single_page ">
        <div class="single_page_content">
          <div class="row">

            <?php $tenlt = $loaitin->getLTTL($_GET['theloaiId']);
            if ($tenlt) {
              $index = 0;
              while ($tenlt_e = $tenlt->fetch_assoc()) {
            ?>

                <div class="col-sm-12">

                  <?php
                  $tin_tl = new bantin();
                  $tintllt = $tin_tl->showTinTLLT($_GET['theloaiId'], $tenlt_e['tenloaitin'], 1, 0);
                  if ($tintllt) {

                    while ($e = $tintllt->fetch_assoc()) {
                      if ($index != 0)
                        echo '<hr class="my-5">';
                      $index = 1;
                      echo '<h4><a href="loaitinpage.php?loaitinId=' . $tenlt_e['loaitinId'] . '" style="color:blue">' . $tenlt_e['tenloaitin'] . '</a></h4>';
                  ?>

                      <div class="row tintop_big" style="margin-top: 15px;">
                        <div class="col-sm-12 col-md-7 text-left">
                          <a href="tin.php?tinId=<?= $e['tinId'] ?>" class="tintop"><?php
                                                                                    if ($e['linkvideo'] == NULL) echo ' <img alt="" src="admin/upload/images/' . $e['hinh'] . '">';
                                                                                    else echo '<span>' . $e['linkvideo'] . '</span>'
                                                                                    ?> </a>
                        </div>
                        <div class="col-sm-12 col-md-5 px-0">
                          <h3> <a href="tin.php?tinId=<?= $e['tinId'] ?>"> <?php echo $e['tieude']; ?></a></h3>
                          <?php echo $e['tomtat'] ?>
                        </div>
                      </div>
                  <?php  }
                  } ?>

                  <?php
                  $tintllt = $tin_tl->showTinTLLT($_GET['theloaiId'], $tenlt_e['tenloaitin'], 4, 1);
                  if ($tintllt) {
                    while ($e = $tintllt->fetch_assoc()) {

                  ?>


                      <div class="col-sm-12">
                        <hr class="my-5">
                        <div class="row list_tin">
                          <div class="col-sm-4 text-left" style="padding: 0;"> <a href="tin.php?tinId=<?= $e['tinId'] ?>" class="hinh_tin">
                              <?php
                              if ($e['linkvideo'] == NULL) echo ' <img alt="" src="admin/upload/images/' . $e['hinh'] . '">';
                              else echo '<span>' . $e['linkvideo'] . '</span>'
                              ?> </a>
                          </div>
                          <div class="col-sm-8">
                            <h4> <a href="tin.php?tinId=<?= $e['tinId'] ?>"> <?php echo $e['tieude'] ?></a></h4>
                            <?php echo $e['tomtat'] ?>
                          </div>
                        </div>
                      </div>

                  <?php
                    }
                    echo '<div style="text-align:right;"><a style="color:red" href="loaitinpage.php?loaitinId=' . $tenlt_e['loaitinId'] . '">Xem thêm</a></div>';
                  }

                  ?>

                </div>
            <?php

              }
            } ?>

            <div class="social_link">
              <hr class="my-5">
              <ul class="sociallink_nav">
                <li>
                  <a href="https://www.facebook.com/Huy.Amory.1238/"><i class="fa fa-facebook"></i></a>
                </li>
                <li>
                  <a href="#"><i class="fa fa-twitter"></i></a>
                </li>
                <li>
                  <a href="#"><i class="fa fa-google-plus"></i></a>
                </li>
                <li>
                  <a href="#"><i class="fa fa-linkedin"></i></a>
                </li>
                <li>
                  <a href="#"><i class="fa fa-pinterest"></i></a>
                </li>
              </ul>
            </div>

            <div class="related_post">
              <h2>Bài viết nổi bật <i class="fa fa-thumbs-o-up"></i></h2>
              <ul class="spost_nav wow fadeInDown animated">
                <?php

                $listnb = $tin_tl->showTinPopTLLT($_GET['theloaiId'], 3);
                if ($listnb) {
                  while ($listnb_e = $listnb->fetch_assoc()) {

                ?>
                    <li>
                      <div class="media">
                        <a class="media-left" href="tin.php?tinId=<?= $listnb_e['tinId'] ?>">
                          <img src="admin/upload/images/<?php echo $listnb_e['hinh'] ?>" alt="" />
                        </a>
                        <div class="media-body">
                          <a class="catg_title" href="tin.php?tinId=<?= $listnb_e['tinId'] ?>"><?php echo $listnb_e['tieude'] ?>
                          </a>
                        </div>
                      </div>
                    </li>
                <?php
                  }
                }
                ?>
              </ul>
            </div>
          </div>

        </div>
      </div>



    </div>



    <div class="col-lg-4 col-md-4 col-sm-4">
      <aside class="right_content">
        <div class="single_sidebar">
          <h2><span>Tin xem nhiều</span></h2>
          <ul class="spost_nav">
            <?php
            $tin_xn = $tin_tl->showTinXemN($_GET['theloaiId']);
            if ($tin_xn) {
              while ($tin_xn_e = $tin_xn->fetch_assoc()) {

            ?>
                <li>
                  <div class="media wow fadeInDown">
                    <a href="tin.php?tinId=<?= $tin_xn_e['tinId'] ?>" class="media-left">
                      <img alt="" src="admin/upload/images/<?php echo $tin_xn_e['hinh'] ?>" />
                    </a>
                    <div class="media-body">
                      <a href="tin.php?tinId=<?= $tin_xn_e['tinId'] ?>" class="catg_title">
                        <?php echo '<b>' .  $tin_xn_e['tieude'] . '</b>';
                        echo '<span style="font-size:12px">' . $tin_tl->textShorten($tin_xn_e['tomtat'], 100) . '</span>';
                        ?>
                      </a>
                    </div>
                  </div>
                </li>
            <?php
              }
            } ?>

          </ul>
        </div>
        <div class="single_sidebar">
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
              <a href="#subcategory" aria-controls="home" role="tab" data-toggle="tab">Loại tin</a>
            </li>
            <li role="presentation">
              <a href="#video" aria-controls="profile" role="tab" data-toggle="tab">Video</a>
            </li>
            <li role="presentation">
              <a href="#comments" aria-controls="messages" role="tab" data-toggle="tab">Bình luận</a>
            </li>
          </ul>
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="subcategory">
              <ul>
                <?php
                $tenlt = $loaitin->getLTTL($_GET['theloaiId']);
                if ($tenlt) {
                  while ($tenlt_e = $tenlt->fetch_assoc()) {
                    echo '<li class="cat-item"><a href="loaitinpage.php?loaitinId=' . $tenlt_e['loaitinId'] . '">' . $tenlt_e['tenloaitin'] . '</a></li>';
                  }
                }
                ?>


              </ul>
            </div>
            <div role="tabpanel" class="tab-pane" id="video">
              <div class="vide_area">
                <iframe width="100%" height="250" src="https://www.youtube.com/embed/AHRzZFXCnxk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="comments">
              <ul class="spost_nav">
                <li>
                  <div class="media wow fadeInDown">
                    <a href="single_page.html" class="media-left">
                      <img alt="" src="public/images/huy.jpg" />
                    </a>
                    <div class="media-body">
                      <a href="single_page.html" class="catg_title">
                        Aliquam malesuada diam eget turpis varius 1</a>
                    </div>
                  </div>
                </li>

              </ul>
            </div>
          </div>
        </div>
        <div class="single_sidebar wow fadeInDown">
          <h2><span>Sponsor</span></h2>
          <a class="sideAdd" href="#"><img src="./public/images/sponsor.jpg" alt="" /></a>
        </div>

        <div class="single_sidebar wow fadeInDown">
          <a class="sideAdd" href="#"><img src="public/images/trex.gif" alt=""></a>
          <a class="sideAdd" href="#"><img src="public/images/tanminhchi.gif" alt=""></a>
          <a class="sideAdd" href="#"><img src="public/images/tiger.gif" alt=""></a>
          <a class="sideAdd" href="#"><img src="public/images/cotden.gif" alt=""></a>
        </div>

      </aside>
    </div>
  </div>
</section>

<?php include './includes/footer.php' ?>