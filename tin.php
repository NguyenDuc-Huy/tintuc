<?php

include './includes/header.php';
include './classes/binhluan_class.php';
include './classes/docgia_class.php';
$binhluan = new binhluan();
?>
<!-- Tính số lượt xem bài viết -->
<?php

$sessionKey = 'post_' . $_GET['tinId'];
if (!isset($_SESSION[$sessionKey])) { // nếu chưa có session
    $_SESSION[$sessionKey] = 1; //set giá trị cho session
    $bantin->updateLuotXem($_GET['tinId']);
}

if (isset($_POST['submit'])) {
    if (!isset($_SESSION['userId'])) {
        echo '<script>window.location.href="login.php";</script>';
    } else {
        $noidung = $_POST['noidung'];

        $bl_res = $binhluan->themBL($_SESSION['userId'], $_GET['tinId'], $noidung);
        $_POST['submit'] = null;
    }
}
?>


<section id="contentSection">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="left_content">
                <div class="single_page">

                    <ol class="breadcrumb">
                        <?php

                        $tin = $bantin->getTin($_GET['tinId']);
                        if ($tin) {
                            $res_tin = $tin->fetch_assoc();
                            $tinTL = $theloai->getTL($res_tin['theloaiId']);
                            if ($tinTL) {
                                $res_TL = $tinTL->fetch_assoc();
                                echo '<li><a href="theloaipage.php?theloaiId=' . $res_tin['theloaiId'] . '">' . $res_TL['tentheloai'] . '</a></li>';
                            }
                            $tinLT = $loaitin->getLT($res_tin['loaitinId']);
                            if ($tinLT) {
                                $res_LT = $tinLT->fetch_assoc();
                                echo '<li><a href="loaitinpage.php?loaitinId=' . $res_tin['loaitinId'] . '">' . $res_LT['tenloaitin'] . '</a></li>';
                            }
                        }
                        ?>

                    </ol>

                    <div class="post_commentbox">
                        <a href="#"><i class="fa fa-eye"></i><?php echo $res_tin['soluotxem']; ?></a>
                        <span><i class="fa fa-calendar"></i><?php echo $res_tin['ngaytao']; ?></span>
                        <a href="#"><i class="fa fa-tags"><?php echo $res_LT['tenloaitin']; ?></i></a>
                    </div>

                    <h1 style="margin-top:10px"><?php
                                                if ($tin) {
                                                    echo $res_tin['tieude'];
                                                }
                                                ?>
                    </h1>

                    <div class="single_page_content">
                        <blockquote>
                            <?php echo $res_tin['tomtat'] ?>
                        </blockquote>
                        <div style="text-align: justify;"><?php echo $res_tin['noidung'] ?></div>
                        <div style="text-align: justify;"><?php echo $res_tin['linkvideo'] ?></div>

                    </div>
                    <div class="social_link">
                        <ul class="sociallink_nav">
                            <li>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </li>
                        </ul>
                    </div>

                    <div class="contact_area">
                        <h4>Ý kiến</h4>
                        <?php if (isset($bl_res)) {
                            echo $bl_res;
                        } ?>
                        <form action="" method="POST" class="contact_form">

                            <textarea name="noidung" class="form-control" cols="30" rows="3" placeholder="Ý kiến của bạn..."></textarea>
                            <input type="submit" name="submit" value="Gửi">
                        </form>
                    </div>

                    <div class="binhluan">
                        <ul class="spost_nav">
                            <?php
                            $bl = $binhluan->showBLTheoTin($_GET['tinId']);
                            if ($bl) {
                                while ($bt_e = $bl->fetch_assoc()) {
                            ?>
                                    <li>
                                        <div class=" row">
                                            <?php
                                            $user = new docgia();
                                            $dg = $user->getDG($bt_e['userId']);
                                            if ($dg) {
                                                $dg_e = $dg->fetch_assoc();

                                            ?>
                                                <div class="col-sm-1"> <img style="border-radius: 50%;" width="40" alt="" src="public/images/<?php echo $dg_e['anhdaidien'] ?>"></div>

                                                <div class=" col-sm-11">
                                                    <strong><?php echo $dg_e['name'] ?></strong> &nbsp; <small style="color:grey"><?php echo $bt_e['ngaytao'] ?></small>
                                                    <div><?php echo $bt_e['noidung'] ?></div>
                                                    <small style="color:grey"><i class="fa fa-thumbs-up"></i>&nbsp;&nbsp;&nbsp;<a href="#">Trả lời</a>&nbsp;&nbsp;&nbsp;<a href="#">Chia sẻ</a></small>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </li>
                            <?php }
                            } ?>
                            <li>
                                <div class=" row">
                                    <div class="col-sm-1"> <img style="border-radius: 50%;" width="40" alt="" src="public/images/duchuy.jpg" /></div>

                                    <div class="col-sm-11">
                                        <strong>nguyen duc huy</strong> &nbsp; <small style="color:grey">2021-10-02 10:42:03</small>
                                        <div>Hay lắm</div>
                                        <small style="color:grey"><i class="fa fa-thumbs-up"></i>&nbsp;&nbsp;&nbsp;<a href="#">Trả lời</a>&nbsp;&nbsp;&nbsp;<a href="#">Chia sẻ</a></small>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="related_post">
                        <h3>Bài viết khác</h3>
                        <hr>
                        <ul class="spost_nav wow fadeInDown animated">

                            <?php
                            $tinkhac = $bantin->showTinLQ($res_tin['loaitinId'], 3, 1);
                            if ($tinkhac) {
                                while ($tinkhac_e = $tinkhac->fetch_assoc()) {

                            ?>
                                    <li>
                                        <div class="media">
                                            <a class="media-left" href="tin.php?tinId=<?= $tinkhac_e['tinId'] ?>">
                                                <?php if ($tinkhac_e['hinh'] != NULL) { ?>
                                                    <img src="admin/upload/images/<?php echo $tinkhac_e['hinh'] ?>" alt="" />
                                                <?php } else echo '<img src="public/images/amthuc.jpg" alt="" />';
                                                ?>
                                            </a>
                                            <div class="media-body">
                                                <a class="catg_title" href="tin.php?tinId=<?= $tinkhac_e['tinId'] ?>">
                                                    <?php echo $tinkhac_e['tieude'] ?></a>
                                            </div>
                                        </div>
                                    </li>
                            <?php }
                            } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
            <aside class="right_content">
                <div class="single_sidebar">
                    <h2><span>Bài viết xem nhiều</span></h2>

                    <ul class="spost_nav">
                        <?php
                        $tinxn = $bantin->tinxemnhieu_loai($res_tin['loaitinId']);
                        if ($tinxn) {
                            while ($tinxn_e = $tinxn->fetch_assoc()) {
                        ?>

                                <li>
                                    <div class="media wow fadeInDown">
                                        <a href="tin.php?tinId=<?= $tinxn_e['tinId'] ?>" class="media-left">
                                            <?php if ($tinxn_e['hinh'] != NULL) { ?>
                                                <img alt="" src="admin/upload/images/<?php echo $tinxn_e['hinh'] ?>" />
                                            <?php } else echo '<img src="public/images/amthuc.jpg" alt="" />' ?>
                                        </a>
                                        <div class="media-body">
                                            <a href="tin.php?tinId=<?= $tinxn_e['tinId'] ?>" class="catg_title">
                                                <?php echo $tinxn_e['tieude'] ?>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                        <?php   }
                        } ?>
                    </ul>
                </div>
                <div class="single_sidebar">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#category" aria-controls="home" role="tab" data-toggle="tab">Loại tin</a>
                        </li>
                        <li role="presentation">
                            <a href="#video" aria-controls="profile" role="tab" data-toggle="tab">Video</a>
                        </li>
                        <li role="presentation">
                            <a href="#comments" aria-controls="messages" role="tab" data-toggle="tab">Bình luận</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="category">
                            <ul>
                                <?php
                                $lt = $loaitin->getLTTL($res_tin['theloaiId']);
                                if ($lt) {
                                    while ($lt_e = $lt->fetch_assoc()) {
                                        echo ' <li class="cat-item"><a href="loaitinpage.php?loaitinId=' . $lt_e['loaitinId'] . '">' . $lt_e['tenloaitin'] . '</a></li>';
                                    }
                                }
                                ?>

                            </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="video">
                            <div class="vide_area">
                                <iframe width="100%" height="250" src="http://www.youtube.com/embed/h5QWbURNEpA?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="comments">
                            <ul class="spost_nav">
                                <?php
                                $bl = $binhluan->showBLTheoTin($_GET['tinId']);
                                if ($bl) {
                                    while ($bt_e = $bl->fetch_assoc()) {
                                ?>
                                        <li>
                                            <div class=" row">
                                                <?php
                                                $user = new docgia();
                                                $dg = $user->getDG($bt_e['userId']);
                                                if ($dg) {
                                                    $dg_e = $dg->fetch_assoc();

                                                ?>
                                                    <div class="col-sm-2"> <img style="border-radius: 50%;" width="40" alt="" src="public/images/<?php echo $dg_e['anhdaidien'] ?>"></div>

                                                    <div class=" col-sm-10">
                                                        <strong><?php echo $dg_e['name'] ?></strong> &nbsp; <small style="color:grey"><?php echo $bt_e['ngaytao'] ?></small>
                                                        <div><?php echo $bt_e['noidung'] ?></div>
                                                        <small style="color:grey"><a href=""><i class="fa fa-thumbs-up"></i></a><a style="margin: 0 10px" href="#">Trả lời</a><a href="#">Chia sẻ</a></small>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </li>
                                <?php }
                                } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="single_sidebar wow fadeInDown">
                    <h2><span>Sponsor</span></h2>
                    <a class="sideAdd" href="#"><img src="./public/images/sponsor.jpg" alt="" /></a>
                </div>

                <div class="single_sidebar wow fadeInDown">
                    <h2><span>Các báo điện tử</span></h2>
                    <ul>
                        <li><a href="https://vietnamnet.vn/vn/tin-moi-nong/">Báo Vietnam net</a></li>
                        <li><a href="https://baomoi.com/">Báo mới</a></li>
                        <li><a href="https://thanhnien.vn/">Báo thanh niên</a></li>
                        <li><a href="https://tinmoi.vn/">Tin mới</a></li>
                        <li><a href="https://vnexpress.net/">VnExpress</a></li>
                        <li><a href="https://tuoitre.vn/">Báo tuổi trẻ</a></li>

                    </ul>
                </div>
                <div class="single_sidebar wow fadeInDown">

                    <a class="sideAdd" href="#"><img src="public/images/cotden.gif" alt=""></a>
                </div>
            </aside>
        </div>
    </div>
</section>

<?php include './includes/footer.php' ?>